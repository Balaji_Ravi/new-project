App = Ember.Application.create();

App.Router.map(function() {
  this.route('diffR');
  this.route('tokenR');
  this.route('register_application');
  this.route('createR');
  this.resource('viewR',function(){
	this.resource('addR');
	this.resource('renameR');
  });

});
//////////////////////////-----  Routes -------- ///////////////////////////////////////////////
App.AddRRoute =Ember.Route.extend({
	model()
	{
		return {
			Title:"Add to file",
			Servlet_name:"add"
			}
	}
});

App.ViewRRoute = Ember.Route.extend({
	model()
	{
		return{		
			Title:"Viewing file",
			Servlet_name:"files"
		}
	}
});

App.DiffRRoute =Ember.Route.extend({
	model()
	{
		return {
			Title:"Difference Finder",
			Servlet_name:"diff",
			}
	}
});

App.CreateRRoute = Ember.Route.extend({

});

/////////////////////////////------ Controllers ------ ////////////////////////////////////////////


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    APPLICATION CONTROLLER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

App.ApplicationController =Ember.Controller.extend({
	access:null,
	gapi:null,
	current_action:'get code',
	init:function() {
		let name = localStorage.name;
		localStorage.Application_name = "demoapp3";
		if(name=='null' || name == null)
		{
			console.log("a");
			this.set('access',null);
		}
		else
		{
			this.set('access',true);
			this.set('user_name',name);
			if(!localStorage.called)
				this.loginuser(name);

		}
		if(localStorage.code==null)
			this.set('code',false);
		else
			this.set('code',true);
		$(".del_each").hide();
		$(".save").hide();

	},
	loginuser:function(name)
	{
		try{
			let obj = new XMLHttpRequest();
			obj.onreadystatechange=function(){
				if(obj.readyState == 4)
				{
					let json=JSON.parse(obj.responseText);
					console.log(json);
					if(obj.status==200)
					{
						localStorage.userid=json.id;
						localStorage.signtype = json.signtype;
						localStorage.called = true;
					}
				}
			}

			let params = "method=createUser&user_name="+name+"&signtype="+localStorage.signin_type;
			params = "users.do?"+params;
			obj.open("GET",params);
			obj.send();

		}
		catch(e){console.log(e);}

	},
	actions:{
		mergefiles : () =>{
			try{
			let obj=new XMLHttpRequest();
			obj.onreadystatechange = ()=>{
				if((obj.readyState==4 && obj.status ==200))
				{
					 alert(obj.responseText);
					 document.location.reload(true);	

				}
				else if(obj.readyState ==4 && obj.status ==401)
				{
					alert(obj.responseText);
				}
			
			}
			let params = "method=merge";
			params = "merge.do"+"?"+params;
			obj.open("GET",params);
			
			obj.setRequestHeader('Accept','text/plain,text/html');
			obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			obj.setRequestHeader('Authorization',localStorage.access_token);
			//obj.setRequestHeader('auth_token',localStorage.auth_token);
			obj.send();
			}
			catch(e){alert(e);}
		},
		signout:function(){
	   		localStorage.name=null;
	   		name=null;
			this.set("access",false);
			document.location.reload(true);   
			window.open('https://mail.google.com/mail/logout?hl=en','popup','width=600,height=600');
			
		},
		pressed:function(){
			this.set("win_press",true);
		},
		winlogin:function(){
			if(localStorage.client_id)
			{
				let win_domain = this.get('win_domain');
				let win_password = this.get('win_password');
				let win_user_name = this.get('win_user_name');
				let flag = false;

				if(win_domain!=null)
					win_domain = win_domain.trim();
				else
					flag = false;
				if(win_password!=null)
					win_password = win_password.trim();
				else
					flag = false;
				if(win_user_name!=null)
					win_user_name=win_user_name.trim();
				else
					flag = false;

				if(win_domain.length>0 && win_password.length>0 && win_user_name.length>0)
				{
					//alert(win_domain);
					try{
						let jsonobj={
							"username": win_user_name,
							"password": win_password,
							"domain": win_domain
						};
						let credential_json = JSON.stringify(jsonobj);
						let params = "credential="+credential_json+"&method=winLogin";
						let obj = new XMLHttpRequest();
						obj.onreadystatechange = ()=>{
							if(obj.readyState==4 && obj.status==200){
								let response = obj.responseText;
								response = JSON.parse(response);
								console.log(response.message);
								if(response.message=="valid")
								{
									alert("user verified");
									this.set("access",true);
									localStorage.signin_type = "windows";
									localStorage.name=this.win_user_name;
									document.location.reload(true);
								}
								else{
									alert("login error");
									this.set("access",false);
								}
							}
						}
						
						obj.open("POST","winlogin.do");
						obj.setRequestHeader('Accept','text/plain,text/html');
						obj.setRequestHeader('Content-type','application/x-www-form-urlencoded');
						obj.send(params);
					}
					catch(e)
					{console.log(e);}
				}
				else
					alert("fill all the fields");
			}
			else
				alert("please register and then proceed");
		},
		registeredUser:function(){
			try{
				let obj=new XMLHttpRequest();
				obj.onreadystatechange = ()=>{
					if(obj.readyState==4 && obj.status ==200)
					{
						 let json = JSON.parse(obj.responseText);
						 
						 if(jQuery.isEmptyObject(json))
						 {
							 alert("you haven't registered yet.  Click register");
						 }	 
						 else
						 {
						 localStorage.client_id = json.client_id;
						 localStorage.client_secret = json.client_secret;
						 localStorage.scope = json.scopes;
						 alert("welcome back\n now you can sign in to the app");
						 }
					}
				}
				let qparam = "client/getcredentials.do"+"?"+"method=client_method";
				obj.open("GET",qparam);
				obj.setRequestHeader('Accept','application/json');
				obj.setRequestHeader('action','get');
				obj.send();
			}
				catch(e){alert(e);}
			},
		
		getcode:function(){
			let self = this;
			try{
				let obj=new XMLHttpRequest();
				obj.onreadystatechange = ()=>{
					if(obj.readyState==4 && obj.status ==200)
					{
						 let json = JSON.parse(obj.responseText);	
						 console.log(json); 
						 localStorage.code = json.code;
						 alert("access code received exchange it for token");
						 self.set('code',true);
					}
				}
				let qparam = "auth.do" + "?" + "method=auth";
				obj.open("GET",qparam);
				obj.setRequestHeader('Accept','application/json');
				obj.setRequestHeader('scope',localStorage.scope);
				obj.setRequestHeader('response_type','code');
				obj.setRequestHeader('client_id',localStorage.client_id);
				obj.send();
				
			}
				catch(e){alert(e);}	
		},
		gettoken:function(){
			let self = this;
			if(localStorage.code)
			{
				try{
					let obj = new XMLHttpRequest();
					obj.onreadystatechange=()=>{
						if(obj.readyState ==4 && obj.status == 200)
						{
							let json = JSON.parse(obj.responseText);
							console.log(json);
							localStorage.access_token=json.access_token;
							document.location.reload(true);
							alert("token received \n expires in "+json.expiresin+"seconds");
							
						}

					}
					let qparam = "method=client_method";
					obj.open("POST","client/getcredentials.do");
					obj.setRequestHeader('Content-type','application/x-www-form-urlencoded');
					obj.setRequestHeader('code',localStorage.code);
					obj.setRequestHeader('Accept','application/json');
					obj.setRequestHeader('action','get_token');
					obj.send(qparam);
				}
				catch(e){console.log(e);}
			}
			
		}
		
		
	}
});
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   ADD CONTROLLER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

App.AddRController = Ember.Controller.extend({
	init:function(){
		if(localStorage.name=='null'||localStorage.name==null){
			this.set("access",null);
			console.log("null access");
		}
		else{
			console.log("true access in add");
			this.set("access",true);
		}
		
		
	},
	needs:'viewR',
	actions:{

		addtofiles:function(){
			let filename = this.get('filename');
			
			let name = this.get('name');
			let id = this.get('id');
			let pwd = this.get('pwd');
			let flag = true;
			let self = this;
			if(name!=null)
				name=name.trim();
			else
				flag=false;
			if(id!=null)	
				id=id.trim();
			else
				flag=false;
			if(pwd!=null)
				pwd=pwd.trim();
			else
				flag =false;

			if(flag){
			if(name.length>0 && id.length>0 && pwd.length>0)
			{
				filename = this.get('controllers.viewR').get('current_filename');
				filename=filename.trim();
				try{
				let obj=new XMLHttpRequest();
				obj.onreadystatechange = ()=>{
					if(obj.readyState==4 && obj.status ==200)
					{
						 this.set('message',obj.responseText);	
						 self.get('controllers.viewR').send('viewfile',filename);
					}
					else if(obj.readyState ==4 && obj.status ==401)
					{
					alert(obj.responseText);
					}
				} 

				var jsonobj ={
					"filename" : filename,
					"name" : name,
					"id" : id,
					"pwd" : pwd
				};
				var myJSON = JSON.stringify(jsonobj);
				console.log(myJSON);
				let params = "json="+myJSON+"&method=add";
				let url = this.content.Servlet_name+"/"+filename+".do";

				obj.open("POST",url);
				obj.setRequestHeader('Accept','text/plain');
				obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				obj.setRequestHeader('Authorization',localStorage.access_token);
				//obj.setRequestHeader('Authorisation',token);
				obj.send(params);
				}
				catch(e){ alert(e); }
			}
			else
				alert("fill all the fields");
		}
		else
			alert("fill all the fields");
		}		
	}	
});
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  create    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
App.CreateRController = Ember.Controller.extend({

access:null,
init : function(){
	let name = localStorage.name;
		if(name=='null' || name == null)
		{
			console.log("a");
			this.set('access',null);
		}
		else
		{
			this.set('access',true);
			this.getfiles(localStorage.userid);
		}	
},
getfiles:function(id)
{
	let self = this;
	try{
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = ()=>{
			if(xhr.readyState==4 && xhr.status==200)
			{
				let json = xhr.responseText;
				console.log(json);
				json = JSON.parse(json);
				self.set("files",json);
				json=[];
			}
		}
		let params ="method=getfiles&userid="+id;
		let url = "users.do"+"?"+params;
		xhr.open("GET",url);
		xhr.send();

	}
	catch(e)
	{e.printStackTrace();}
},
actions:
{
	create_file:function()
	{
		let self = this;
		let filename = this.get('filename');
		let valid = validate_data(filename);
		if(valid == "ok")
		{
			//put .csv checking here
			try{
				let xhr = new XMLHttpRequest();
				xhr.onreadystatechange = ()=>{
					if(xhr.readyState==4)
					{
						alert(xhr.responseText);
						self.getfiles(localStorage.userid);
					}
				}
				let param = "method=createfile&filename="+filename+"&uid="+localStorage.userid;
				let url = "users.do"+"?"+param;
				xhr.open("GET",url);
				xhr.send();
			}
			catch(e)
			{console.log(e);}

		}
		else{
			alert(valid);
		}
	
	},
	view_permissions:function(){
		alert("hi");
	}
}
});



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   VIEW CONTROLLER  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

App.ViewRController = Ember.Controller.extend({  
	isedit:false,
	edited_lines:[], 
	init:function(){
		if(localStorage.name=='null'||localStorage.name==null){
			this.set("access",null);
			console.log("null access");
		}
		else{
			console.log("true access");
			this.set("access",true);
			this.get_fileslist();	
		}
	},
	get_fileslist:function(){
		try{
			let self = this;
			let obj = new XMLHttpRequest();
			obj.onreadystatechange = ()=>{
				if(obj.readyState==4 && obj.status ==200)
				{
					let json;
					 try{
						json = JSON.parse(obj.responseText);
						self.set('files',json);
						console.log(json);
					 }
					 catch(e){alert(obj.responseText);}			
				}
				else if(obj.readyState ==4 && obj.status ==401)
				{
						alert(obj.responseText);
				}
			}
			let url = "fileops.do"+"?method=getfiles";
			obj.open("GET",url);
			obj.setRequestHeader('action','getfiles');
			obj.send();
			}
			catch(e){alert(e)};

	},
	addchecked:function(datas)
	{
		let data;
		for(i=0;i<datas.length;i++)
		{
			
			datas[i]["checked"]=false;
		}
		this.set('datas',datas);
		//console.log(datas);
	},
	getfile:function(filename){
		let self = this;
		this.set("current_filename",filename);
		$(".del_each").hide();
		try{
			let obj= new XMLHttpRequest();
			obj.onreadystatechange=function(){
			if(obj.readyState==4)
			{
				if(obj.status ==200)
				{
					//console.log("two  "+this);
					let json=obj.responseText;
					//console.log(json);
					try{
					data = JSON.parse(json);
					
					self.set('datas',data);	
					self.set('message',"");
					}
					catch(e)
					{
						self.set('message',json);
						alert(json);
						self.set('datas',null);	
						console.log("error in viewing");
						console.log(e);
					}
				}
			}
			}
			let url = this.content.Servlet_name+"/"+filename+".do"+"?method=view";
			obj.open("GET",url);
			obj.setRequestHeader('Accept','text/plain');
			obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			obj.setRequestHeader('Authorization',localStorage.access_token);
			obj.send()

		}
		catch(e){
			alert(e);
		}


	},
	deleach:function(arg)
	{
		let line = arg.line;
		let name = arg.name;	
		let id = arg.id;
		let self = this;
		let pwd = arg.pwd;
		var jsonobj ={
			"line" : line,
			"name" : name,
			"id" : id,
			"pwd" : pwd,
			"filename":this.get('current_filename')
		};
		try
		{
			var myJSON = JSON.stringify(jsonobj);
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange=function(){
				if(xhr.status=200 && xhr.readyState==4)
				{
					let response = xhr.responseText;	
					let msg = "received response ==== "+response;
					console.log(msg);
					self.set('delete_response',response);
					return response;

				}
			}
			let url = "fileops.do?method=delete";
			let param = "json="+myJSON;
			xhr.open("POST",url);
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send(param);
		}
		catch(e)
		{console.log(e);}	

		

	},
	editline:function(){
		let self = this;
		let edit_array = this.get('edited_lines');
		let filename = this.get('current_filename');
		let empty =[];
		edit_list=[];
		
		if(edit_array.length==0)
			alert("no changes are made");
		else
		{
			let i;
			
			let myJSON = JSON.stringify(edit_array);
			try{
				let xhr = new XMLHttpRequest();
				xhr.onreadystatechange=function(){
					if(xhr.status=200 && xhr.readyState==4)
					{
						alert(xhr.responseText);
						//self.viewfile(filename);
						//console.log(self);
					//	document.location.reload(true);
							self.set('edited_lines',[]);
							self.getfile(filename);
					}
				}
				let url = "fileops.do?method=edit";
				let param = "json="+myJSON;
				xhr.open("POST",url);
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				xhr.send(param);
			}
			catch(e)
			{console.log(e);}	
		}
	},
	actions:{
		test:function(line)
		{
			let msg = "prev  "+this.get('edited_lines');
			console.log(msg);
			if(!this.get('edited_lines').includes(parseInt(line,10)))
				this.get('edited_lines').push(parseInt(line,10));
			console.log(this.get('edited_lines'));
			
		},
		save:function()
		{
		let count = 1;
		let filename = this.get('current_filename');
		var arrData=[];
		// loop over each table row (tr)
		let skip = true;
		let edited_lines =edit_list;
		console.log(edited_lines);
		$(".viewtable tr").each(function(){
		var currentRow=$(this);
		
		if(!skip)
		{
		if(edited_lines.includes(count))
		{
		
        let name=currentRow.find("td:eq(0)").text();
        let id=currentRow.find("td:eq(1)").text();
        let pwd=currentRow.find("td:eq(3)").text();

		 var obj={};
		 if(validate_data(name)!="ok")
			{alert("invalid name entered");
			return;
			}
		else if(validate_data(id)!="ok")
			{
			alert("invalid id entered");
			return;
			}
		else if(validate_data(pwd)!="ok")
			{
			alert("invalid password entered");
			return;
			}
		else{
			obj.line=count.toString();
			obj.name=name;
        	obj.id=id;
			obj.pwd=pwd;
			obj.filename=filename;
			arrData.push(obj);
		}
		}
		count++;
	}
	skip = false;
       // arrData.push(obj);
	});
	console.log(arrData);
	this.set('edited_lines',arrData);
	
	if(arrData.length>0)
		{
			arrData=[];
		this.editline();
		}
	else
		alert("no changes were made");
	},
	viewfile:function(element){
		let filename = element;
		this.set('current_filename',filename);
		let count=0;
		let data;
		var self=this;
		console.log(this);
		$(".editfile").hide();
		$(".editdiv").hide();
		$(".del_each").hide();
		if (filename!=null)
		{
			filename=filename.trim();
			if(filename.length>0)
			{
				
				try{
				let obj= new XMLHttpRequest();
				obj.onreadystatechange=function(){
					if(obj.readyState==4)
					{
					if(obj.status ==200)
					{
						//console.log("two  "+this);
						self.set("edit",true);
						$('.edit_menu').show();
						let json=obj.responseText;
						//console.log(json);
						try{
						$('.Fops').css('display','block');
						data = JSON.parse(json);
						self.set('datas',data);	
						self.addchecked(data);
						self.set('message',"");
						}
						catch(e)
						{
							self.set('message',json);
							alert(json);
							self.set('datas',null);	
							console.log("error in viewing");
							console.log(e);
						}
					}
					else if(obj.status ==401)
					{
					//alert("i'm here");
						alert(obj.responseText);
					}
					else if(obj.status==404)
					{
						self.set("edit",false);
						$('.edit_menu').hide();
						self.set('datas',null);	
						self.set('message',obj.responseText);
					}
					else
					self.set('datas',null);	
				}
				}

				let url = this.content.Servlet_name+"/"+filename+".do"+"?method=view";
				obj.open("GET",url);
				obj.setRequestHeader('Accept','text/plain');
				obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				obj.setRequestHeader('Authorization',localStorage.access_token);
				obj.send();

				}
				catch(e){alert(e);}
			}
			else
				alert("enter filename");
			}
			else
				alert("enter filename");
			
		},
		filename(element)
		{
			console.log(element);
			this.set('filename',element);
			this.viewfile();
		},
		
		edit:function(arg)
		{
			this.set('selected',true);
			if(arg == "init")
			{
				//$(".editfile").css("display","block");
				$(".renamediv").hide();
				$(".del_each").hide();
				$(".save").show();
			}
		},
		delete:function(arg)
		{
			let msg = [];
			let self = this;
			let i;
			if(arg == "init")
			{
				$(".del_each").show();
				$(".save").hide();
			}
			else
			{
				let proceed = confirm("do you want to delete the field ?");
				if(proceed==true)
				{
					let length = this.get('datas').length;
					for(i=0;i<length;i++)
					{
						let each = this.get('datas')[i];
						
						if(each.checked)
						{	
							let line = each.line;
							let name = each.name;	
							let id = each.id;
							let pwd = each.pwd;
							var jsonobj ={
								"line" : line,
								"name" : name,
								"id" : id,
								"pwd" : pwd,
								"filename":this.get('current_filename')
							};
							
							msg.push(jsonobj);
						}
						
						
					}
					msg = JSON.stringify(msg);
					console.log(msg);

						//=================================================================
					try
					{
						let xhr = new XMLHttpRequest();
						xhr.onreadystatechange=function(){
							if(xhr.status=200 && xhr.readyState==4)
							{
								response = xhr.responseText;
								console.log(response);
								self.set("res",response);
								self.getfile(self.get('current_filename'));
							}
						}
						let url = "fileops.do?method=delete";
						let param = "json="+msg;
						xhr.open("POST",url);
						xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
						xhr.send(param);
					}
					catch(e)
					{console.log(e);}
					
					//console.log(response);
							
				}
				
			}
					
		}
			
		}
		
	


});

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIFFERENCE CONTROLLER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
App.RenameRController =Ember.Controller.extend({
	needs:'viewR',
	init:function(){
		this.set('viewCont',this.get('controllers.viewR'));
		$(".del_each").hide();
		$(".save").hide();
	},
	
	actions:{
		rename:function(cmd)
		{
			let self =this;
			if(cmd == 'init')
			{
				console.log("just");
				//this.set('ok_rename',true);
				$('.renamediv').show();
				$(".editfile").hide();
				$(".save").hide();
			}
			else{
				
				let new_name = this.get('newfile_name');
				if(new_name!=null)
				{
					new_name = new_name.trim();
					
					if(new_name.length>0 && new_name.indexOf(' ') == -1)
					{
						
						let oldname = this.get('viewCont').get('current_filename');
						if(new_name!=oldname)
						{
							//alll
							try{
								let xhr = new XMLHttpRequest();
								xhr.onreadystatechange=function(){
									if(xhr.status==200 && xhr.readyState ==4)
									{
										alert(xhr.responseText);
										self.get('viewCont').send('get_fileslist');
										self.get('viewCont').send('getfile',new_name);
									}	
								}
								let params="&oldname="+oldname+"&newname="+new_name;
								let url ="fileops.do?method=renameFile"+params;
								xhr.open("GET",url);
								xhr.setRequestHeader('Accept','text/plain');
								//obj.setRequestHeader('Authorization',localStorage.access_token);
								xhr.send();
							}
							catch(e)
							{console.log(e);}
						}	
						else{
							alert("provide a different name");
						}
					}
					else
					{
						alert("fill the fields no empty spaces");
					}
				}
				else
					alert("fill the field");
				//alert(new_name+' ==== '+oldname);
			}
		}
	}

});




//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
App.DiffRController =Ember.Controller.extend({
	
	init:function()
	{
		
		if(localStorage.name=='null'||localStorage.name==null)
		{
			this.set("access",null);
			console.log("null access");
		}
		else{
			console.log("true access");
			this.set("access",true);
		}
		try{
		let obj = new XMLHttpRequest();
		obj.onreadystatechange = ()=>{
			if(obj.readyState==4 && obj.status ==200)
			{
				let json;
				 try{
					console.log(this);
				 	json=JSON.parse(obj.responseText);
				 	this.set("init_old",json.old);
				 	this.set("init_new",json.new);
				 }
				 catch(e){alert(obj.responseText);}			
			}
			else if(obj.readyState ==4 && obj.status ==401)
			{
					alert(obj.responseText);
			}
		}
		obj.open("GET","d.do?method=diff");
		obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		obj.setRequestHeader('Authorization',localStorage.access_token);
		obj.send();
		}
		catch(e){alert(e)};
	},
	actions:{
		diffe :function(){
			var self = this;
			console.log(this);
			try{

			let obj = new XMLHttpRequest();
			obj.onreadystatechange = ()=>{
				if(obj.readyState==4 && obj.status ==200)
				{
					 try{
					 	
					 	let json=JSON.parse(obj.responseText);
					 	self.set("sequence",json.sequence);
					 	self.set("removed",json.remove);
					 	self.set("message",json.message);
					 	let olddata = json.old;
					 	let newdata = json.new;
					 	let tempseq = json.sequence.concat();
					 	let temprem = json.remove.concat();
					 	let diffarr = [];
					 	
					 	let blankjson = {
					 		"value": 0,
					 		"index": 0
					 	};
					 	for(let i=0;i<tempseq.length;i++)
					 	{
					 		let diff = temprem[i] - tempseq[i];

					 		if(diff < 0)
					 		{
					 			diff = diff * -1;
					 			let index = temprem[i]-1;

					 			for(let j=0; j<diff; j++)  
					 			{
					 				olddata.splice(index, 0, blankjson);
					 			}

					 			for(let k=i; k<temprem.length; k++)
					 			{
					 				temprem[k] = temprem[k] + diff;
					 			}
					 		}

					 		else if(diff > 0)
					 		{
					 			let index = tempseq[i]-1;

					 			for(let j=0; j<diff; j++)  
					 			{
					 				newdata.splice(index, 0,blankjson);
					 			}

					 			for(let k=i; k<tempseq.length; k++)
					 			{
					 				tempseq[k] = tempseq[k] + diff;
					 			}

					 		}
					 	console.log("temprem "+temprem);
					 	console.log("tempseq "+tempseq);
					 	}

					 	self.set("olddata",olddata);
					 	self.set("newdata",newdata);

					 	console.log(olddata);
					 	console.log(newdata);

					 	//console.log(json.message);
					 }
					 catch(e){console.log(e);
					 console.log(obj.responseText);}			
				}
				else if(obj.readyState ==4 && obj.status ==401)
				{
					alert(obj.responseText);
				}
			}
			let qparam = "d.do?method=diff";
			obj.open("GET",qparam);
			obj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			obj.setRequestHeader('Authorization',localStorage.access_token);
			obj.send();
			}
			catch(e){alert(e);}
		}
	}
});

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TOKEN CONTROLLER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

App.TokenRController =Ember.Controller.extend({
	init:function()
	{
		if(localStorage.name=='null'||localStorage.name==null)
		{
			this.set("access",null);
			console.log("null access");
		}
		else{
			console.log("true access");
			this.set("access",true);
			this.set("token",localStorage.auth_token);
		}
	}

});

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  REGISTERING AUTH CONTROLLER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

App.RegisterApplicationController = Ember.Controller.extend({
	init:function(){
		if(localStorage.name=='null'||localStorage.name==null)
			this.set("access",null);
		else
			this.set("access",true);
		
	},
	
	actions:
	{
		addscopes:function()
		{
			console.log(this);
			let scope="";
			if(this.get('add'))
				scope+="add ";
			if(this.get('merge'))
				scope+="merge ";
			if(this.get('viewing'))
				scope+="view ";
			if(this.get("diff"))
				scope+="diff";
			try{
				let obj = new XMLHttpRequest();
				obj.onreadystatechange=()=>{
				
					if(obj.status == 200 && obj.readyState==4){
					
					let json = JSON.parse(obj.responseText);
					let message = json.message;
					if(message == "ok")	
					{
						localStorage.registered = 'registered';
						localStorage.client_id = json.client_id;
						localStorage.client_secret = json.client_secret;
						let msg =  localStorage.client_id+" "+localStorage.client_secret;
						localStorage.scope = scope;
						store_in_client_db();
						alert("Registered");
						console.log(msg);
						
					}
					else if(message=="already registered")
					{
						alert("already registerd");
						localStorage.registered = 'registered';
					}
					else
					{
						alert("error in registering");
					}

					this.transitionToRoute('index');
					}
				}
				let param = "register.do"+"?"+"method=register";
				obj.open("GET",param);
				obj.setRequestHeader("clientname",localStorage.Application_name);
				obj.setRequestHeader("scopes",scope);
				
				obj.send();
			}
			catch(e){alert(e);}
			
		
		}
	}

});
//%%%%%%%%%%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX%%%%%%%%%%%%
///////////////////////////---------- helpers ------------/////////////////////////////////

Ember.Handlebars.helper("isremoved",function(index,removed_list,value){

	let innerhtml;
	if(value=="")
		value = "<br/>";
	if(index==0)
	{
		innerhtml = "<tr><td bgcolor='black'> <br/> </td></tr>"
	}
	else if(removed_list){
		if(removed_list.includes(index))
			innerhtml = "<tr ><td >"+value+"</td></tr>";
		else
			innerhtml = "<tr ><td bgcolor='#ff9999' >"+value+"</td></tr>";
	}
	return new Ember.Handlebars.SafeString(innerhtml);

});



Ember.Handlebars.helper("isadded",function(index,sequence_list,value){
	//console.log("is added"+sequence_list+"   "+index);
	let innerhtml;
	if(value=="")
		value = "<br/>";
	if(index==0)
	{
		innerhtml = "<tr><td bgcolor='black'><br/></td></tr>"
	}

	else if(sequence_list)
	{
		if(sequence_list.includes(index))
			innerhtml="<tr><td>"+value+"</td></tr>";
		else
			innerhtml="<tr><td bgcolor='#71da71'>"+value+"</td></tr>";

	}
	return new Ember.Handlebars.SafeString(innerhtml);
	
});


Ember.Handlebars.helper("isnull",function(value){
	//console.log("is added"+sequence_list+"   "+index);
	if(value=="")
		return new Ember.Handlebars.SafeString("<br/>");
	else
		return value;
	
});

Ember.Handlebars.helper("accesscheck",function(reference){
	let temp = localStorage.name;
	if(temp=='null')
		temp = null;
	if(temp!=null)
	{
		reference.set("access",true);
		reference.set("user_name",temp);
	}
	else
	{
		reference.set("access",false);
		reference.set("user_name",null);
		name=null;
	}
	let text ="In helper , access= "+reference.access+" ,user_name = "+reference.user_name;
	console.log(text);
});

/////////////////////////////////////////////   functions //////////////////////////////////////////////////////////////////////////////
let signin_type = null;
let name=null;
function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
function onSigIn(googleUser) {

	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId());
	console.log('Name: ' + profile.getName());
	console.log('Email: ' + profile.getEmail());
	name = profile.getName();
	localStorage.name=name;
	localStorage.signin_type = "google";
	if(localStorage.client_id)
		 document.location.reload(true); 
	else
		alert("please register and then proceed");
}


function logout(){
	var auth2 = gapi.auth2.getAuthInstance();
	
    auth2.signOut().then(function () {

	auth2.disconnect();
	console.log('User signed out.');
	localStorage.clear();
	name=null;
	document.location.reload(true);
	document.location.href="/demo6/";
	});
}
 function store_in_client_db()
 {
	 try{
	let obj = new XMLHttpRequest();
	let param = "client/getcredentials.do"+"?"+"method=client_method";
		obj.open("GET",param);
		obj.setRequestHeader('Accept','application/json');
		obj.setRequestHeader('action','store');
		obj.setRequestHeader('client_id',localStorage.client_id);
		obj.setRequestHeader('client_secret',localStorage.client_secret);	
		obj.setRequestHeader('scope',localStorage.scope);
		obj.send();
		}
		catch(e){alert(e)};
 }

 function validate_data(data)
 {
	
	if(data==null)
	{
		console.log(data);
		return "null";

	}
	else
	{
		data.trim();
		if(data.length>0 && data.indexOf(" ")==-1)
			return "ok";
		else
		{
			console.log(data);
			return "contains spaces";
		}
	}
 }
 let edit_list=[]
 function test(line)
 {
	
	var i = $(line).find("td:eq(2)").text();
	i = parseInt(i);

	if(!edit_list.includes(i))
	{
		edit_list.push(i);
	}
	console.log(edit_list);
 }
