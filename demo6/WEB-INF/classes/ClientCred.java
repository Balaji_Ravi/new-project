import java.sql.*;
import java.util.*;

class ClientCred 
{
    String getcred() throws SQLException
    {
        String json= "{}";
        Connection con = new GetCon().getConnection();
        Statement stmt = con.createStatement();
        String query = "select client_id,client_secret,scopes from clientdb";
        ResultSet rs = stmt.executeQuery(query);
        if(rs.next())
            json = "{\"client_id\":\""+rs.getString(1)+"\",\"client_secret\":\""+rs.getString(2)+"\",\"scopes\":\""+rs.getString(3)+"\"}";
        con.close();
        return json;
        

    }

    void store(String id,String secret,String scope) throws SQLException
    {
        Connection con = new GetCon().getConnection();
        PreparedStatement pstmt = con.prepareStatement("truncate table clientdb");
        pstmt.executeUpdate();
        pstmt = con.prepareStatement("insert into clientdb values(?,?,?)");
        pstmt.setString(1,id);
        pstmt.setString(2,secret);
        pstmt.setString(3,scope);
        pstmt.executeUpdate();
        con.close();
    }
}