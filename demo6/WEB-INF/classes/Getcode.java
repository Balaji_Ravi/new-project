import java.util.*;
import java.net.*;

class Getcode
{
    private final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!@#%*[]()<>~|+-_=";
        
    static public void  main(String[] args) {
      //  new Getcode().getcode(8);
      
    }
        
    String generatecode(int size){
        String code = null;
    try{
        
        StringBuilder builder = new StringBuilder();
        while (size-- != 0) {
        int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
        builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        code = builder.toString();
        
    }
    catch(Exception e){e.printStackTrace();}
    return code;
    }
}