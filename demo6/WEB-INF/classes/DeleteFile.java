import java.io.*;
import java.util.*;
import java.sql.*;

public class DeleteFile 
{
    static String filename="";
    static String name="";
    static String pwd="";
    static String id="";
    static int line=0;
    static Properties prop = null;
    static String deletingline = null;
    static String csvdata = null;

    public String delete(String json)
    {
        try{
            Converter conv = new Converter();
            conv.json2java_del(json);          //parses and sets the static values of the class
            String config_path = "D:\\Apache Software Foundation\\webapps\\demo6\\WEB-INF\\classes\\config.properties";
            FileInputStream input = new FileInputStream(config_path);
            prop = new Properties();
            prop.load(input);
            input.close();

            String filepath = "";
            boolean db_check = true;
            if(prop.getProperty("sourcename").equals(filename))
            {
                db_check =false;
                filepath = prop.getProperty("source");
            }
            else if(prop.getProperty("targetname").equals(filename))
                filepath = prop.getProperty("target");

            csvdata=name+","+id+","+pwd;
            
            DeleteFile obj = new DeleteFile();
            String res = null;
            if(db_check){
                String hash = String.valueOf(csvdata.hashCode());
                if(obj.delete_from_db(hash)>0 )
                {
                    res="ok";                
                }
                else
                {
                    System.out.println("error in deleting in DB");
                     return "error";
                }
            }
            if(obj.delete_in_file(filepath))
            {
                return "ok";
            }
            else{
                System.out.println("error in file deleting");
                return "error";
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }

    public int delete_from_db(String hash)
    {
        int res=0;
        Connection con = null;
        try{
            con = new GetCon().getConnection();
            String table_name = prop.getProperty("tname");
            PreparedStatement pstmt = con.prepareStatement("delete from "+table_name+" where hash = ? limit 1");
            pstmt.setString(1,hash);
            System.out.println("query ===  "+pstmt.toString());
            res = pstmt.executeUpdate();
            pstmt.close();
            con.close(); 
            return res;
        }
        catch(Exception e)
        {e.printStackTrace();
        return 0;}
    }



    int getLineNo(String filepath,String csvdata)
    {
        int count=0;
        boolean avail = false;
        try{
            RandomAccessFile raf = new RandomAccessFile(filepath, "rw");
            String line="";
           
            System.out.println("\nCSVDATA ===  "+csvdata+"\n");
            while((line=raf.readLine())!=null)
            {
                count++;
                if(line.equals(csvdata)) 
                {
                    avail = true;
                    break;
                }
                
            }    
            System.out.println("data line" +count);
            raf.close();
        
        }
        catch(Exception e)
        {e.printStackTrace();} 
        if(!avail)
            count = -10;
        return count;   
    }

    boolean delete_in_file(String filepath)
	{
		try{
            RandomAccessFile raf = new RandomAccessFile(filepath, "rw");
                // Leave the n first lines unchanged.
            
            int toRemove = (new DeleteFile().getLineNo(filepath,csvdata))-1;
            if(toRemove<0)
            {
                raf.close();
                System.out.println("\nfile not found\n");
                return false;
            }
            System.out.println("line to be removed ==== "+ toRemove);
            for (int i = 0; i < toRemove; i++)
                raf.readLine();

            // Shift remaining lines upwards.
            long writePos = raf.getFilePointer();
            raf.readLine();
            long readPos = raf.getFilePointer();

            byte[] buf = new byte[1024];
            int n;
            while ((n = raf.read(buf))!= -1) {
            //  System.out.println(buf);
                raf.seek(writePos);
                raf.write(buf, 0, n);
                readPos += n;
                writePos += n;
                raf.seek(readPos);
            }
            raf.setLength(writePos);
            raf.close();
            return true;
	    }
        catch(Exception e)
        {e.printStackTrace();
        return false;}
        }

}