import java.util.*;
import java.io.*;
import java.util.Properties;

class D4jni
{
	
	static int original_nooflines=0;
	static int new_nooflines=0;
	static int[] sequence;
	static int[] remove;
	String json="";

	public static void main(String[] args) {
		D4jni obj =new D4jni();
		System.out.println(obj.senddiff());
	}

	String senddiff(){
	D4jni obj = new D4jni();
	Fileops fobj = new Fileops();
	try
	{
		Properties prop = new Properties();
		InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties");
		prop.load(input);

		String original_filepath =  prop.getProperty("old");
		String new_filepath = prop.getProperty("new");



		System.out.println("paths  "+original_filepath+"--------"+new_filepath);
		sequence = fobj.getnewseq(original_filepath, new_filepath);
		remove = fobj.getoldseq();

///////////////////////////---------------json Conversion------------------//////////////////////////
		

		//json="{\"sequence\":"+seq+",\"remove\":"+remove+",\"old\":[";

		json="{\"sequence\":[";
		for(int i=0;i<sequence.length;i++)			//   ADDING SEQUENCE LISTS TO JSON 
		{
			json+=String.valueOf(sequence[i]);
			if(i!=sequence.length-1)
				json+=",";
		}
		json+="],\"remove\":[";
		for(int i=0;i<remove.length;i++)
		{
			json+=String.valueOf(remove[i]);
			if(i!=remove.length-1)
				json+=",";
		}
		json+="],\"old\":[";


		//BufferedReader br2 = new BufferedReader(new FileReader(oldf));
		String oline="";
		int count=1;
		int lastline = fobj.getnooflines(original_filepath);		// ADDING ORIGINAL FILE VALUES TO JSON
		original_nooflines = lastline;
		fobj.createpath1(original_filepath);
		while((oline=fobj.getfile1())!=null)
		{
			if((oline.substring(oline.length()-1)).equals("\n")) 	// REMOVING '\n' CHARACTER AT THE END OF THE EACH LINE OBTAINED
				oline=oline.substring(0,oline.length()-1);
			json+="{\"value\":\""+String.valueOf(oline)+"\",\"index\":"+(count)+"}";
			if(count!=lastline)
				json+=",";
			count++;
			
		}
		//br2.close();
		count=1;
		json+="],\"new\":[";

		//br2 = new BufferedReader(new FileReader(newf));
		oline="";
		String nline=null;
		lastline = fobj.getnooflines(new_filepath);			// ADDING CHANGED FILE VALUES TO JSON
		new_nooflines = lastline;
		fobj.createpath1(new_filepath);
		while((oline=fobj.getfile1())!=null)
		{
			if((oline.substring(oline.length()-1)).equals("\n"))	// REMOVING '\n' CHARACTER AT THE END OF THE EACH LINE OBTAINED
				oline=oline.substring(0,oline.length()-1);
			json+="{\"value\":\""+String.valueOf(oline)+"\",\"index\":"+(count)+"}";
			if(count!=lastline)
				json+=",";
			count++;
			
		}
		//br2.close();
		json+="],";

		String message ="";
		//System.out.println(remove.length+"   "+original_nooflines+"   "+sequence.length+"   "+new_nooflines);
		if(remove.length==original_nooflines && sequence.length==new_nooflines)		//CHECKING IF THEY ARE SAME OR NOT
		{ 
			message ="Identitcal files";
		}
		else
			message = "Changes Occured";
		json+="\"message\":\""+message+"\"}";

		System.out.println(json);


	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return json;
}//senddiff()


}