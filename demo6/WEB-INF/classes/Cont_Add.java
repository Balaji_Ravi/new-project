import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.Properties;

public class Cont_Add extends DispatchAction
{
	public ActionForward add(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
	{

        System.out.println("in controller");
        VerifyUser vobj = new VerifyUser();
		String token = request.getHeader("Authorization");
		String scope ="add";
		PrintWriter pw = response.getWriter();
		String status = "";
		status = vobj.verifytoken(token,scope);
		if(status.equals("ok"))
		{
			
			String json=request.getParameter("json");
			Converter conv = new Converter();
			Details det = new Details();

			det=Converter.json2java(json);

			System.out.println(det.filename+" "+det.name);
			String filename=det.filename;
			String name=det.name;
			String id =det.id;
			String pwd=det.pwd;
			String filepath = null;
			response.setContentType("text/plain");

			Properties prop = new Properties();
			InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties");
			prop.load(input);
					
			if(filename.equals(prop.getProperty("sourcename"))||filename.equals(prop.getProperty("targetname")))
			{
				if(filename.equals(prop.getProperty("sourcename")))
					filepath=prop.getProperty("source");
				else
					filepath=prop.getProperty("target");

				Add obj =new Add();
				int lines=obj.addData(name, id, pwd,filepath);
				if(lines>0)
					pw.write("Inserted Successfully. new Details id = "+lines);
				else
					pw.write("insertion error");
			}
			else
			{
				pw.write("failure wrong filename");
			}
			pw.close();
		}
		else
		{
			response.setStatus(401);
			pw.write(status);
		}
		return null;
    }

}