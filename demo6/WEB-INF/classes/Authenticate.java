import java.util.Properties;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Authenticate extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String response_type = request.getHeader("response_type");
        String client_id = request.getHeader("client_id");
        VerifyUser obj = new VerifyUser();
        String json = null;
        String code = null;
        try{
            PrintWriter pw = response.getWriter();
            if(response_type.equals("code"))
            {
                String scope = request.getHeader("scope");
                if(obj.verify(client_id,scope))
                {
                    System.out.println("client_id and scope verified");
                    code = obj.getcode();
                    obj.storecode(client_id,code);
                    json = "{\"code\":\""+code+"\",\"token_type\":\"Bearer\"}";
                    pw.write(json);
                    
                }
                else
                {
                    json = "{\"error\":\"Not authenticated\"}";
                    pw.write(json);
                }
                VerifyUser.con.close();   
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return;
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter pw = response.getWriter();
        String code =null;
        String json = null;
        VerifyUser obj = new VerifyUser();
        System.out.println(request.getHeader("client_id")+"  "+request.getHeader("client_secret"));
        try
        {
            String response_type = request.getHeader("response_type");
            if(response_type.equals("token"))
            {
                String client_id = request.getHeader("client_id");
                String client_secret = request.getHeader("client_secret");
                code = request.getHeader("code");
                if(obj.verifycode(client_id,client_secret,code))
                {
                    String token = obj.gettoken(obj.getscopes(client_id));
                   // obj.storetoken(client_id,token);
                    json = "{\"access_token\":\""+token+"\",\"expiresin\":\"60\",\"token_type\":\"Bearer\"}";

                }
                else{
                    json = "{\"error\":\"Not authenticated\"}";
                }
              
                pw.write(json);

            }
            VerifyUser.con.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        

    }


}