import java.util.*;
import java.util.Date;

import java.sql.*;
import java.sql.Timestamp;
import java.io.*;
import java.util.Properties;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.*;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.annotation.*;

class VerifyUser
{
    static Connection con = null;
    static Statement stmt = null;
    ResultSet rs = null;
    static InputStream input = null;
    static Properties prop = null;
    VerifyUser(){
        try{
        con = new GetCon().getConnection();
        stmt = con.createStatement();
        input= getClass().getClassLoader().getResourceAsStream("config.properties");
		prop = new Properties();
		prop.load(input);
        }
        catch(Exception e){e.printStackTrace();}
    }

    public static void main(String[] args) {
        //VerifyUser obj = new VerifyUser();
        try{
        //obj.gettoken("all");

        }
        catch(Exception e){e.printStackTrace();}

    }

    boolean verify(String client_id, String scope)
    {
        
        boolean result = false;
        try{
        rs = stmt.executeQuery("select scope from auth where client_id = '"+client_id+"'");
        rs.next();
        String scopes = rs.getString(1);
        String allscopes[] = scopes.split(" ");
        List<String> scope_list = Arrays.asList(allscopes);
        String givenscopes[] = scope.split(" ");
        for(String each:givenscopes)
        {
            if(scope_list.contains(each))
            {
                result = true;
            }
            else
            {
                System.out.println("scope mismatch || "+each+"is not registered");
                result = false;
                break;
            }
        }
        if(result)
            System.out.println("client and scope verified");
        else
            System.out.println("invalid scope or user");
        }
        catch(Exception e){e.printStackTrace();}
        return result;
    }

    String getcode()
    {
        String code=null;
        int code_length = 8;
        code = new Getcode().generatecode(code_length);
        return code;
    }

    void storecode(String client_id, String code) throws SQLException
    {
        //stmt.executeUpdate("update tokens set code = '"+code+"' where client_id='"+client_id+"'");
        PreparedStatement pstmt = con.prepareStatement("update tokens set code = ? where client_id = ?");
        pstmt.setString(1, code);
        pstmt.setString(2, client_id);
        int i = pstmt.executeUpdate();
        System.out.println(i+" rows updated code");
    }

    boolean verifycode(String client_id, String client_secret, String code) throws SQLException
    {
       PreparedStatement pstmt = con.prepareStatement("select client_secret from auth where client_id = ?");
       pstmt.setString(1,client_id);
       rs = pstmt.executeQuery();
       rs.next();
       boolean result = false;

       if(rs.getString(1).equals(client_secret))
       {
            pstmt = con.prepareStatement("select code from tokens where client_id = ?");
            pstmt.setString(1,client_id);
            rs = pstmt.executeQuery();
            rs.next();
            if(rs.getString(1).equals(code))
            {
                System.out.println("code matched");
                result = true;
            }
            else
            {
                System.out.println("code mismatch");
            }
       }
       else
       {
           System.out.println("invalid secret code");
       }

       return result;
    }

    String gettoken(String scope)
    {
        String token=null;
        try{
            Algorithm algorithmHS = Algorithm.HMAC256(prop.getProperty("secret_key"));
			Date issued = new Date();
			Date expires = new Date();
			Calendar cal = Calendar.getInstance();
            issued = cal.getTime();
            int expiry_seconds = Integer.parseInt(prop.getProperty("expiry_duration"));
		    cal.add(Calendar.SECOND,expiry_seconds);
		    expires = cal.getTime();
		   
		    System.out.println("gettoken "+expires.getTime()+"   "+issued.getTime());
			token = JWT.create().withIssuer("auth0").withSubject(scope).withIssuedAt(issued).withExpiresAt(expires).sign(algorithmHS);
        }
        catch(Exception e){e.printStackTrace();}
        System.out.println("TOKEN   "+token);
        return token;
    }

    void storetoken(String client_id,String token) throws SQLException
    {
        PreparedStatement pstmt = con.prepareStatement("update tokens set access_token = ?,time = ? where client_id = ?");
        pstmt.setString(1, token);
        Timestamp ts1 = new Timestamp(new Date().getTime());
        pstmt.setTimestamp(2,ts1);
        pstmt.setString(3, client_id);
        int i =pstmt.executeUpdate();
        if(i>0)
            System.out.println("token updated");

    }

    String getscopes(String client_id) throws SQLException
    {
        String scopes="";
        PreparedStatement pstmt = con.prepareStatement("select scope from auth where client_id = ? ");
        pstmt.setString(1,client_id);
        rs = pstmt.executeQuery();
        rs.next();
        scopes = rs.getString(1);
        return scopes;

    }
    String verifytoken(String token,String scope)
    {
        String result ="";
        DecodedJWT jwt = null;
        try{
          
            System.out.println("recieved token "+token);
            Algorithm algorithmHS = Algorithm.HMAC256(prop.getProperty("secret_key"));
            JWTVerifier ver = JWT.require(algorithmHS).withIssuer("auth0").build();
            jwt = ver.verify(token);

            String scopes = jwt.getSubject();
            String allscopes[] = scopes.split(" ");
            List<String> scope_list = Arrays.asList(allscopes);
            for(String x : scope_list)
            {
                System.out.print(x+"---");

            }
            System.out.println("received "+scope);
            if(!scope_list.contains(scope))
            {   
                System.out.println("scope not found");
                result = "not authorised for this function";
                return result;
            }
            result = "ok";
        }
        catch(JWTDecodeException e)
        {
            result = "INVALID TOKEN RECEIVED";
            System.out.println("THE TOKEN IS INVALID");
            return result;
        }
        catch(TokenExpiredException e)
        {
            result = "TOKEN GOT EXPIRED. GET A NEW TOKEN";
            System.out.println("THE TOKEN GOT EXPIRED");
            return result;
        }
        catch(Exception e)
        {
            result = "invalid JWT token ";
            e.printStackTrace();
            return result;
        }
        
        return result;
    }

   void close()
   {
       try{
       stmt.close();
       con.close();
       }
       catch(Exception e)
       {e.printStackTrace();}

   }
    
}
