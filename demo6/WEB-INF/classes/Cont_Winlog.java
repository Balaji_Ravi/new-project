import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_Winlog extends DispatchAction
{
    private static String user_name="";
	private static String password="";
    private static String domain="";
    
    public ActionForward winLogin(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
		String json = request.getParameter("credential");
		System.out.println(json);
		Converter.winconvert(json);
		try{
			System.out.println("in win login "+user_name+"==="+password);
			boolean result = Login.authWindowsUser(user_name,domain,password);
			response.setContentType("text/plain");
			PrintWriter pw = response.getWriter();

			if(result)
			{
				pw.write("{\"message\":\"valid\"}");
			}
			else
			{
				pw.write("{\"message\":\"error\"}");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
        }
        return null;
    }

    
	void setfields(String uname,String pass,String dom)
	{
		user_name=uname;
		password=pass;
		domain=dom;
	}
}