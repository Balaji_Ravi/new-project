import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_Users extends DispatchAction
{
    private String name,id,signtype;
    
   
    public ActionForward createUser(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        Acops obj = new Acops();
        PrintWriter pw = response.getWriter();
        name = request.getParameter("user_name");
        id = obj.getid();
        signtype = request.getParameter("signtype");
        String json ="";
        if(obj.isexisting_User(name).equals("no"))
        {
            System.out.println("new user");
            obj.add_User_to_Db(name,id,signtype);
            obj.create_Tables_for_user(id);
            json = obj.getUserdetails(name);
        }
        else if(obj.isexisting_User(name).equals("yes"))
        {   System.out.println("existing user");
            // pw.write("welcome back "+name);
             json = obj.getUserdetails(name);
        }
        pw.write(json);
        return null;
    }

    public ActionForward createfile(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        Acops obj = new Acops();
        PrintWriter pw = response.getWriter();
        id = request.getParameter("uid");
        String filename = request.getParameter("filename");
        if(obj.validfile(filename))
        {
            String file_id = obj.gener_fileid();
            obj.add_file_to_files(filename,file_id);
            obj.add_own_file(id,file_id);
            pw.write("files added");
        }
        else
        {
            pw.write("file already exists give a new name");
        }
        pw.close();
        return null;
    }

    public ActionForward getfiles(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        Acops obj = new Acops();
        PrintWriter pw = response.getWriter();
        pw.write(obj.get_user_files(request.getParameter("userid")));
        pw.close();
        return null;

    }
}