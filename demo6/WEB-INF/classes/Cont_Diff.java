import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;

public class Cont_Diff extends DispatchAction
{
    public ActionForward diff(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
    
        System.out.println("Indiffer");
		VerifyUser vobj = new VerifyUser();
		String token = request.getHeader("Authorization");
		String scope ="diff";
		PrintWriter pw= response.getWriter();

		String status = "";
		status = vobj.verifytoken(token,scope);
		if(status.equals("ok"))
		{
			D4jni obj = new D4jni();
			String result="";

			String path = request.getPathInfo();
			System.out.println("path ===== "+path);
			result=obj.senddiff();
			response.setContentType("text/plain");
			pw.write(result);
		}
		else
		{
			response.setStatus(401);
			pw.write(status);
		}
		vobj.close();
		pw.close();
        return null;
    }
}