import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_Auth extends DispatchAction
{
    
    public ActionForward auth(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        String response_type = request.getHeader("response_type");
        VerifyUser obj = new VerifyUser();
        if(response_type.equals("code"))
        {
            String client_id = request.getHeader("client_id");
           
            String json = null;
            String code = null;
            try{
                PrintWriter pw = response.getWriter();
                
                    String scope = request.getHeader("scope");
                    if(obj.verify(client_id,scope))
                    {
                        System.out.println("client_id and scope verified");
                        code = obj.getcode();
                        obj.storecode(client_id,code);
                        json = "{\"code\":\""+code+"\",\"token_type\":\"Bearer\"}";
                        pw.write(json);
                        
                    }
                    else
                    {
                        json = "{\"error\":\"Not authenticated\"}";
                        pw.write(json);
                    }
                
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(response_type.equals("token"))
        {
            PrintWriter pw = response.getWriter();
            String code =null;
            String json = null;
            System.out.println(request.getHeader("client_id")+"  "+request.getHeader("client_secret"));
            try
            {
                
                String client_id = request.getHeader("client_id");
                String client_secret = request.getHeader("client_secret");
                code = request.getHeader("code");
                if(obj.verifycode(client_id,client_secret,code))
                {
                    String token = obj.gettoken(obj.getscopes(client_id));
                // obj.storetoken(client_id,token);
                    json = "{\"access_token\":\""+token+"\",\"expiresin\":\"60\",\"token_type\":\"Bearer\"}";

                }
                else{
                    json = "{\"error\":\"Not authenticated\"}";
                }
            
                pw.write(json);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            
        }
        obj.close();
        return null;

    }
}