import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_Client extends DispatchAction
{
       
    public ActionForward client_method(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        String action = request.getHeader("action");
        if(action.equals("store"))
        {
            try{
                ClientCred obj = new ClientCred();
                String client_id = request.getHeader("client_id");
                String client_secret = request.getHeader("client_secret");
                String scope = request.getHeader("scope");
                obj.store(client_id,client_secret,scope);
            }
            catch(Exception e){e.printStackTrace();}
        }
        else if(action.equals("get"))
        {
            try{

                ClientCred obj = new ClientCred();
                String msg = null;
                msg = obj.getcred();
                System.out.println("in client credentials \t "+msg);
                PrintWriter pw = response.getWriter();
                pw.write(msg);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }  
        }
        else{
            try{
                Request robj = new Request();
                //String url = request.getHeader("url");
                String method = request.getParameter("method");
                System.out.println("========="+method);
                String code = request.getHeader("code");
                String resp = robj.call(code,method);
                PrintWriter pw = response.getWriter();
                pw.print(resp);
                }
                catch(Exception e)
                {e.printStackTrace();}
        }
        return null;
    }
}