import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_Register extends DispatchAction
{
    private String client_id = "";
	private String client_secret ="";
    public ActionForward register(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        PrintWriter pw= response.getWriter();
		response.setContentType("text/html");
		String client_name = request.getHeader("clientname");
		String scopes =  request.getHeader("scopes");
		System.out.println("client_namesss  "+client_name);
		RegisterUser reg = new RegisterUser();
		if(reg.existinguser(client_name))
		{
			pw.write("{\"message\":\"already registered\"}");
		}
		else
		{
			client_id = reg.getid();
			client_secret = reg.getsecret();

			if(reg.register(client_id,client_secret,scopes,client_name))
				{
					String resp = "{\"message\":\"ok\",\"client_id\":\""+client_id+"\",\"client_secret\":\""+client_secret+"\"}";
					pw.write(resp);
					System.out.println(resp);
				}
			else
				pw.write("{\"message\":\"error in registering\"");

        }
        
        return null;

    }
}
