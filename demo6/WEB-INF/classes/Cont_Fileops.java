import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;
import java.util.*;

public class Cont_Fileops extends DispatchAction
{

    public ActionForward getfiles(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        PrintWriter pw = response.getWriter();
        InputStream input= getClass().getClassLoader().getResourceAsStream("config.properties");
		Properties prop = new Properties();
        prop.load(input);
        input.close();
        File folder = new File(prop.getProperty("sourceFolderPath"));
        String filenames ="";
        filenames = "[";
        for (File file : folder.listFiles())
        {
            filenames+="\""+file.getName()+"\",";   
        }
        filenames = filenames.substring(0,filenames.length()-1);
        filenames+="]";
        System.out.println("in fileops\n"+filenames);
        pw.write(filenames);   
        return null;    
    }
    public ActionForward renameFile(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
       // File conf = new File("config.properties");
        String config_path = "D:\\Apache Software Foundation\\webapps\\demo6\\WEB-INF\\classes\\config.properties";
        System.out.println("paaath ======"+config_path);
        String oldname = request.getParameter("oldname");
        String newname = request.getParameter("newname");

        PrintWriter pw = response.getWriter();
        
		FileInputStream input = new FileInputStream(config_path);
		
		Properties prop = new Properties();
        prop.load(input);
        input.close();
        
        String filepath =  prop.getProperty("sourceFolderPath")+"\\";
		System.out.println(filepath+oldname);

		File old = new File(filepath+oldname);
        File nf = new File(filepath+newname);
        //File conf = new File("config.cfg");
       // String config_path = conf.getAbsolutePath();

        if(old.renameTo(nf))
        {
			FileOutputStream outs = new FileOutputStream(config_path);

            
            if(oldname.equals(prop.getProperty("targetname")))
            {
                prop.setProperty("targetname",newname);
                prop.setProperty("target",filepath+newname);
                System.out.println("new field == "+prop.getProperty("target"));
				
            }
            else if(oldname.equals(prop.getProperty("sourcename")))
            {
                prop.setProperty("sourcename",newname);
				prop.setProperty("source",filepath+newname);
				
                System.out.println("new field == "+prop.getProperty("source"));
            } 
            prop.store(outs,null);
            
            pw.write("file renamed");
            outs.close();
           // pw.write("file name changed , refresh to see the changes");
		}  
        else
           pw.write("renaming process error");
        
           return null;
            
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
   {
        PrintWriter pw = response.getWriter();
        String json = request.getParameter("json");
        EditFile obj = new EditFile();

        json = json.substring(1,json.length()-1);
         // System.out.println("in Controller "+json);
          String prev =  String.valueOf(json.charAt(0));
          String buf="";
          ArrayList<String> li = new ArrayList<String>();
  
          for(int j =0; j<json.length();j++)
          {
            //System.out.println("hi");
              String i = String.valueOf(json.charAt(j));
              if(prev.equals("}") && i.equals(","))
              {
                System.out.println(buf+"hi");
                  li.add(buf);
                  buf="";
              }
              else
              {
                  buf+=i;
                  prev = i;
              }
  
          }
          li.add(buf);
        String each[] = li.toArray(new String[li.size()]);
        System.out.println("length ===="+each.length);
        boolean flag = false;



        for(String s:each)
        {
            if(obj.edit(s).equals("ok"))
            {
                System.out.println("file modified");
                
                flag = true;
            }  
            else{     
            pw.write("\nerror while editing the file " + s);
            flag = false;
            break;
            }
        
        }
        if(flag)
            pw.write("file modified");
        else
            pw.write("error in editing");
        return null; 
        
   }

   public ActionForward delete(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
   {
        PrintWriter pw = response.getWriter();
        String json = request.getParameter("json");
        DeleteFile obj = new DeleteFile();

        json = json.substring(1,json.length()-1);
         // System.out.println("in Controller "+json);
          String prev =  String.valueOf(json.charAt(0));
          String buf="";
          ArrayList<String> li = new ArrayList<String>();
        
          
          for(int j =0; j<json.length();j++)
          {
            //System.out.println("hi");
              String i = String.valueOf(json.charAt(j));
              if(prev.equals("}") && i.equals(","))
              {
                System.out.println(buf+"hi");
                  li.add(buf);
                  buf="";
              }
              else
              {
                  buf+=i;
                  prev = i;
              }
  
          }
          li.add(buf);
        String each[] = li.toArray(new String[li.size()]);
        System.out.println("length ===="+each.length);
        boolean flag = false;
        for(String s:each)
        {
            System.out.println("sending JSON "+s);
            if(obj.delete(s).equals("ok"))
            {
               //
                System.out.println("deleted");
                flag = true;
                //return null;
            }
            else
            {
                System.out.println("ERROR OCCURED IN DELETING "+s);
                flag = false;
                break;
            }
        }
        if(flag)
            pw.write("entity deleted");
        else
            pw.write("error in deleting");
        return null;
          
   }
}