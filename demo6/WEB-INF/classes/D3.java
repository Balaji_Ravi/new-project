import java.util.*;
import java.io.*;
import java.util.Properties;

class D3
{
	static ArrayList<Integer> seq ;
	static ArrayList<Integer> remove;
	static ArrayList<Integer> finalseq;
	static int original_nooflines=0;
	static int new_nooflines=0;
	static boolean stop =false;
	static ArrayList<List> all;
	String json="";

	public static void main(String[] args) {
		D3 obj =new D3();
		System.out.println(obj.senddiff());
	}

	String senddiff(){
	D3 obj =new D3();
	try
	{
		Properties prop = new Properties();
		InputStream input = getClass().getClassLoader().getResourceAsStream("config.cfg");
		prop.load(input);
		File oldf = new File(prop.getProperty("old"));
		File newf = new File(prop.getProperty("new"));

		System.out.println("lines old ="+obj.nooflines(oldf)+"  lines new = "+obj.nooflines(newf));
		original_nooflines = obj.nooflines(oldf);
		new_nooflines = obj.nooflines(newf);
		BufferedReader br1=new BufferedReader(new FileReader(oldf));
	
		String oline="";
		String nline="";

		remove = new ArrayList<Integer>();
		seq = new ArrayList<Integer>();
		finalseq = new ArrayList<Integer>();
		all = new ArrayList<List>();

		RandomAccessFile raf = new RandomAccessFile(newf, "r");
		
		int nlinecount =1;

		while((oline=br1.readLine()) != null)
		{
			nlinecount=1;
			boolean exists =false;
			ArrayList<Integer> li = new ArrayList<Integer>();
			while((nline=raf.readLine()) != null)
			{	
				if(oline.equals(nline))
				{
					li.add(nlinecount);
					exists = true;
				}
				nlinecount++;
			}
			if(!exists)
			{
				li.add(-1);
			}
			all.add(li);
			raf.seek(0);
		}

		System.out.println(all);
		raf.close();
		br1.close();


		
		for(int i=0;i<all.size();i++)
		{
			List<Integer> first = new ArrayList<Integer>();
			first = all.get(i);
			
				if((first.get(0)==-1) && (i == (all.size()-1)))
				{
						break;
				}
				else if(i == (all.size()-1))
				{
					seq.add(first.get(0));
					obj.seqchecker();
				}	
				else if((first.get(0)==-1))
					continue;
				else
				{
					for(int j=0;j<first.size();j++)
					{
					System.out.println("start ====== "+first.get(j));
					seq.clear();
					seq.add(first.get(j));
					obj.order(all.get(i+1),first.get(j),i);
					}
				}
		}
		//remove.clear();
		//System.out.println("sequence== "+finalseq);
		int ind = 0;
		for(int i=0;i<all.size() && ind<finalseq.size();i++)
		{
			List lis = all.get(i);
			if(lis.contains(finalseq.get(ind)))
				{
					System.out.println("ok");
					remove.add(i+1);
					ind++;
				}
		}

		seq=finalseq;
		System.out.println("remove ==="+ remove);





		/////////////////------json Conversion------------------///////////////
		

		json="{\"sequence\":"+seq+",\"remove\":"+remove+",\"old\":[";
		BufferedReader br2 = new BufferedReader(new FileReader(oldf));
		oline="";
		int count=1;
		int lastline = obj.nooflines(oldf);
		while((oline=br2.readLine())!=null)
		{
			json+="{\"value\":\""+String.valueOf(oline)+"\",\"index\":"+(count)+"}";
			if(count!=lastline)
				json+=",";
			count++;
			
		}
		br2.close();
		count=1;
		json+="],\"new\":[";

		br2 = new BufferedReader(new FileReader(newf));
		nline=null;
		lastline = obj.nooflines(newf);
		while((nline=br2.readLine())!=null)
		{
			json+="{\"value\":\""+String.valueOf(nline)+"\",\"index\":"+(count)+"}";
			if(count!=lastline)
				json+=",";
			count++;
			
		}
		br2.close();
		json+="],";

		String message ="";
		if(remove.size()==original_nooflines && seq.size()==new_nooflines)
		{ 
			message ="Identitcal files";
		}
		else
			message = "Changes Occured";
		json+="\"message\":\""+message+"\"}";

		//System.out.println(json);


	}
	
	catch(IOException e)
	{
		e.printStackTrace();
	}
	return json;
}//senddiff()

void order(List<Integer> li, int prev_value, int prev_index)
{
	D3 obj = new D3();
	ArrayList<Integer> copy = new ArrayList<Integer>();
	copy.addAll(seq);
	boolean flag = false;
	boolean islastline = (prev_index == all.size()-2);
		if(li.get(0) == -1 && islastline)
		{
			System.out.println("seq ="+seq);
			obj.seqchecker();
		}
		else if(li.get(0) == -1)
		{
			order(all.get(prev_index+2), prev_value, prev_index+1);
		}
		else if(islastline)
		{
			for(int i=0;i<li.size();i++)
			{
				if(li.get(i) > prev_value )   //&& li.get(i)== new_nooflines
				{
					if(seq.size()==0)
						seq.addAll(copy);	
					seq.add(li.get(i));
				}
				System.out.println("at last line "+seq+"   "+li);
				obj.seqchecker();
			}

		}
		else
		{
			for(int i=0;i<li.size();i++)
			{
				if(li.get(i) > prev_value)
				{	
					flag = true;
					if(seq.size()==0)
					{	
						seq.addAll(copy);
					}
					seq.add(li.get(i));
					order(all.get(prev_index+2),li.get(i),prev_index+1);
				}
				
			}
			if(!flag)
					order(all.get(prev_index+2),prev_value,prev_index+1);
		}

	//for	
	
}

public void seqchecker()
{
	D3 obj = new D3();
	if(seq.size()>0)
	{
		if(seq.size() > finalseq.size())
			{
			//System.out.println(seq.size()+" ==="+finalseq.size());
			finalseq.clear();
			finalseq.addAll(seq);
			//seq.clear();
			if( finalseq.size() == original_nooflines)
	 			stop = true;
			
			}
		else if(finalseq.size()==seq.size())
		{	

			int lastelement = seq.get(seq.size()-1);
			System.out.println(lastelement+" xxxxxxxx "+new_nooflines);
			if(lastelement == new_nooflines && seq.size() == original_nooflines)
			{
				System.out.println("changed");
				finalseq.clear();
				finalseq.addAll(seq);
				//seq.clear();
			}
			
		}
		seq.clear();
	}
	System.out.println(finalseq);
	
}


boolean iscontinuos(List<Integer> seq)
{
	boolean res = false;
	int diff = 0;
	for(int i=0;i<seq.size()-1;i++)
	{
		diff +=(seq.get(i+1)-seq.get(i));
	}
	if(diff == seq.size()-1)
		res =true;
	return res;
}

int nooflines(File file)
{
	int lines =0;
	try{
	
		String l;
		BufferedReader br = new BufferedReader(new FileReader(file));
		while((l=br.readLine())!=null)
			lines++;
    
	}
	catch(Exception e){e.printStackTrace();}

	return lines;
}

}