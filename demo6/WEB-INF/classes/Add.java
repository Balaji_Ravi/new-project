import java.util.*;
import java.io.*;

class Add
{
	
	BufferedWriter bw=null;

	public int addData(String name,String id,String pass,String filepath)
	{
		
		try
		{
			Properties prop = new Properties();
			InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties");
			prop.load(input);

			//bw = new BufferedWriter(new FileWriter(filepath,true));
			Merge obj = new Merge();
			String line=name+","+id+","+pass;

			//bw.write(line);
			//bw.newLine();

			Fileops fileobj = new Fileops();
			fileobj.addtofile(filepath,name,id,pass);
			if(prop.getProperty("target").equals(filepath))
			{
				obj.tablecreate(1);
				obj.insert_to_DB(String.valueOf(line.hashCode()));
			}
			obj.close();
			//bw.close();

			//LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(filepath));
		    //lineNumberReader.skip(Long.MAX_VALUE);
		    //int lines = lineNumberReader.getLineNumber();
		    //lineNumberReader.close();

		    int lines = fileobj.getnooflines(filepath);
			
			return lines;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}

	}
}