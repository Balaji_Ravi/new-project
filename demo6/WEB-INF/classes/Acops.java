import java.util.*;

import java.sql.*;

import java.io.*;

public class Acops

{
    static Connection con = null;
    static Properties prop = null;
    private final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

    Acops()
    {
        try{
        prop = new Properties();
        con = new GetCon().getConnection();
        FileReader input = new FileReader("D:\\Apache Software Foundation\\webapps\\demo6\\WEB-INF\\classes\\config.properties");
        prop.load(input);
        input.close();
        }
        catch(Exception e)
        { e.printStackTrace();}
    }

    String gener_fileid()
    {
        String code = "";
        try{
            while(true)
            {   
                int size = 3;
                StringBuilder builder = new StringBuilder();
                while (size-- != 0) {
                int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
                builder.append(ALPHA_NUMERIC_STRING.charAt(character));
                }
                code = builder.toString();     
                
                PreparedStatement pstmt=con.prepareStatement("select exists (select fid from files where fid=?)");
                pstmt.setString(1,code);
                ResultSet rs = pstmt.executeQuery();
                rs.next();
                if(rs.getString(1).equals("0"))
                 {   
                    pstmt.close();
                    rs.close(); 
                    break; 

                 }     
            }
        }
        catch(Exception e){e.printStackTrace();}
        return code;
    }

    void close()
    {
        try{
            con.close();
        }
        catch(Exception e)
        {e.printStackTrace();}
    }


    String getid(){
        String code = "";
        try{
            while(true)
            {
                
                int size = 3;
                StringBuilder builder = new StringBuilder();
                while (size-- != 0) {
                int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
                builder.append(ALPHA_NUMERIC_STRING.charAt(character));
                }
                code = builder.toString();     
                
                if(verify_id(code))
                    break;

            }
        }
        catch(Exception e){e.printStackTrace();}
        return code;
    }

    boolean verify_id(String code) throws SQLException
    {
        boolean result=false;
        ResultSet rs = null;
        try{
        PreparedStatement pstmt = con.prepareStatement("select exists(select id from "+prop.getProperty("users_tablename")+" where id = ?)");
      //  pstmt.setString(1,prop.getProperty("users_tablename"));
        pstmt.setString(1, code);
        rs = pstmt.executeQuery();
        rs.next();
       
        if(rs.getString(1).equals("0"))
            result = true;
        else
            result = false;
        }
        catch(Exception e)
        {e.printStackTrace();}
        finally{
            rs.close();
        }
        return result;
    }

    boolean add_User_to_Db(String name,String id,String signtype)
    {
        boolean result= false;
        PreparedStatement pstmt = null;
        try{
            pstmt = con.prepareStatement("insert into "+prop.getProperty("users_tablename")+" values(?,?,?)");
            pstmt.setString(1,id);
            pstmt.setString(2,name);
            pstmt.setString(3,signtype);

            if(pstmt.executeUpdate()<0)
                {System.out.println("ERROR IN CREATING USER INTO DB");
                result = false;
            }
            else
            {
                System.out.println("USER CREATED");
                result = true;
            }

            pstmt.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
       
        return result;

    }

    public static void main(String[] args) {
        Acops obj = new Acops();
       // obj.create_Tables_for_user("123");
       try{
       System.out.println(obj.get_user_files("qwi"));
       }
       catch(Exception e){e.printStackTrace();}
    }

    void create_Tables_for_user(String id) 
    {
        PreparedStatement pstmt = null;
        try{
    
            String table_name = "allfiles"+id;
            System.out.println(table_name);

            String sql = "drop table if exists "+table_name;
            pstmt = con.prepareStatement(sql);
            //pstmt.setString(1,table_name);
            pstmt.execute();

            sql = "create table "+table_name+" (fid varchar(3),owns varchar(1),perms varchar(5))";
            pstmt = con.prepareStatement(sql);
           // pstmt.setString(1,table_name);
            pstmt.execute();

            table_name = "share_"+id;
            sql = "drop table if exists "+table_name;
            pstmt = con.prepareStatement(sql);
            pstmt.execute();

            sql = "create table "+table_name+" (fid varchar(3),uid varchar(3),perms varchar(5))";
            pstmt =  con.prepareStatement(sql);
            pstmt.execute();

            pstmt.close();
            System.out.println("table created");
        }
        catch(Exception e){e.printStackTrace();}

    }

    String isexisting_User(String name)
    {
        String res="";
        try{
            PreparedStatement pstmt = con.prepareStatement("select exists(select name from "+prop.getProperty("user_dbname")+" where name = ?)");
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            if(rs.getString(1).equals("1"))
                res = "yes";
            else
                res = "no";
            rs.close();
            pstmt.close();

        }
        catch(Exception e)
        {
            e.printStackTrace();
            res = "error";
        }
        
        return res;
    }

    String getUserdetails(String name) throws Exception
    {
        PreparedStatement pstmt = con.prepareStatement("select id,signtype from "+prop.getProperty("user_dbname")+" where name = ?");
        pstmt.setString(1, name);
        ResultSet rs = pstmt.executeQuery();
        rs.next();

        String json = Converter.java2json_userdetails(rs.getString(1),rs.getString(2));
        pstmt.close();
        rs.close();
        return json;

    }

    boolean validfile(String filename) throws IOException
    {
        filename = prop.getProperty("sourceFolderPath")+"\\"+filename;
        File f = new File(filename);
		if(f.createNewFile())
        {	System.out.println("new file created");
            return true;
        }
        else
        {
            System.out.println("file name already exists");
            return false;
		}
    }
    void add_file_to_files(String filename, String fileid) throws SQLException
    {
        PreparedStatement pstmt = con.prepareStatement("insert into "+prop.getProperty("allfiles_table")+" values(?,?)");
        pstmt.setString(1,fileid);
        pstmt.setString(2,filename);
        if(pstmt.executeUpdate() > 0)
            System.out.println("file inserted");
        else
            System.out.println("file creation error");
        pstmt.close();
    }

    void add_own_file(String user_id,String fileid) throws SQLException
    {
        PreparedStatement pstmt = con.prepareStatement("insert into allfiles"+user_id+" values(?,?,?)");
        System.out.println(pstmt);
        pstmt.setString(1,fileid);
        pstmt.setString(2,"y");
        pstmt.setString(3,"edit");
        if(pstmt.executeUpdate()>0)
            System.out.println("files added to user db");
        else
            System.out.println("error in adding file to user db");
        pstmt.close();
    }


    String get_user_files(String user_id) throws SQLException
    {
        PreparedStatement pstmt = con.prepareStatement("select fid,owns from allfiles"+user_id);
        ResultSet rs = pstmt.executeQuery();
        
        String res = "[";
        while(rs.next())
        {
            ResultSet rs2 = null;
            pstmt = con.prepareStatement("select fname from "+prop.getProperty("allfiles_table")+" where fid = ?");
            pstmt.setString(1,rs.getString(1));
            rs2 = pstmt.executeQuery();
            rs2.next();
           // System.out.println(rs2.getString(1));
            res+="{\"name\":\""+rs2.getString(1)+"\",\"owns\":\""+rs.getString(2)+"\"},";
            rs2.close();
            
        }
       
        pstmt.close();
        rs.close();
        res=res.substring(0,res.length()-1);
        res+="]";

        return res;
        
    }

}