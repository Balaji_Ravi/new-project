import java.util.*;
import java.io.*;

public class Converter{

	static int lines = 1;
	public static String java2json(Details obj){
		String filename=obj.filename;
		String name = obj.name;
		String pwd= obj.pwd;
		String id = obj.id;

		return "{\"filename\":\""+filename+"\",\"name\":\""+name+"\",\"id\":\""+id+"\",\"pwd\":\""+pwd+"\"}" ;
	}

	public static String java2json_view(Details obj){
		String filename=obj.filename;
		String name = obj.name;
		String pwd= obj.pwd;
		String id = obj.id;

		return "{\"line\":\""+(lines++)+"\",\"name\":\""+name+"\",\"id\":\""+id+"\",\"pwd\":\""+pwd+"\"}" ;
	}
	public void resetline()
	{
		lines = 1;
	}
	public static Details json2java(String json){
		Details obj = new Details();
		json = json.substring(1,json.length()-1);
		String fields[]= json.split(",");
		String values[]=new String[4];
		for(int i=0;i<values.length;i++)
		{
			String[] s = fields[i].split(":");
			values[i]=s[1];
		}
		
		obj.filename = values[0].substring(1,values[0].length()-1);
		obj.name = values[1].substring(1,values[1].length()-1);
		obj.id = values[2].substring(1,values[2].length()-1);
		obj.pwd = values[3].substring(1,values[3].length()-1);
		
		return obj;
	}

	public static void winconvert(String json)
	{
		json = json.substring(1,json.length()-1);
		
		String values[] = new String[3];
		String fields[] = json.split(",");
		for(int i =0;i<values.length;i++)
		{
			String[] s = fields[i].split(":");
			values[i]=s[1];
			values[i]=values[i].substring(1,values[i].length()-1); 	//removing quotations(") at the ends
		}
		Cont_Winlog obj = new Cont_Winlog();
		obj.setfields(values[0],values[1],values[2]);
	}
	public static void main(String[] args) {
		new Converter().json2java_edit("{\"line\":\"2\",\"name\":\"2ee\",\"id\":\"2\",\"pwd\":\"2\",\"filename\":\"one.csv\"}");
	}
	public void json2java_edit(String json)
	{
		// line,name ,id ,pwd ,filename
		System.out.println("received json== "+json);

		json = json.substring(1,json.length()-1);
		String fields[]= json.split(",");
		String values[]=new String[fields.length];
		for(int i=0;i<values.length;i++)
		{
			String[] s = fields[i].split(":");
			values[i]=removequotes(s[1]);

			//System.out.println(values[i]);
		}
		EditFile.filename = values[4];
		EditFile.name = values[1];
		EditFile.id = values[2];
		EditFile.pwd = values[3];
		EditFile.line = Integer.parseInt(values[0]);
	}

	public void json2java_del(String json)
	{
		// line,name ,id ,pwd ,filename
		System.out.println("received json== "+json);

		json = json.substring(1,json.length()-1);
		String fields[]= json.split(",");
		String values[]=new String[fields.length];
		for(int i=0;i<values.length;i++)
		{
			String[] s = fields[i].split(":");
			values[i]=removequotes(s[1]);

			//System.out.println(values[i]);
		}
		DeleteFile.filename = values[4];
		DeleteFile.name = values[1];
		DeleteFile.id = values[2];
		DeleteFile.pwd = values[3];
		DeleteFile.line = Integer.parseInt(values[0]);
	}

	

	static String removequotes(String data)
	{
		while(data.charAt(0)=='"')
			data = data.substring(1,data.length()-1);
		return data;
	}

	static String java2json_userdetails(String id,String signtype)
	{
		String json = "{\"id\":\""+id+"\",\"signtype\":\""+signtype+"\"}";
		return json;

	}

}