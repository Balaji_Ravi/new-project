import java.io.*;  
import java.util.*;
  
public class Test2 {  
    
    public static void main(String[] args) throws IOException{  
     try{
        Test2 obj = new Test2();
        
     }
     catch(Exception e)
     {e.printStackTrace();}
}  

static void fun() throws IOException
{
    File inputFile = new File("myFile.txt");
    File tempFile = new File("myTempFile.txt");
    
    BufferedReader reader = new BufferedReader(new FileReader(inputFile));
    BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
    
    String lineToRemove = "bbb";
    String currentLine;
    
    while((currentLine = reader.readLine()) != null) {
        // trim newline when comparing with lineToRemove
        String trimmedLine = currentLine.trim();
        if(trimmedLine.equals(lineToRemove)) continue;
        writer.write(currentLine + System.getProperty("line.separator"));
    }
    writer.close(); 
    reader.close(); 
    boolean successful = tempFile.renameTo(inputFile);
}
}