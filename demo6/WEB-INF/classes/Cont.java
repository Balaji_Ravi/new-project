import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;

public class Cont extends DispatchAction
{
    public ActionForward merge(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {

        System.out.println("in controller");
        VerifyUser vobj = new VerifyUser();
		String token = request.getHeader("Authorization");
		System.out.println("token  "+token);
		String scope ="merge";

		PrintWriter pw= response.getWriter();

		String status = "";
		status = vobj.verifytoken(token,scope);
		if(status.equals("ok"))
		{
			Merge obj =new Merge();
			boolean result=obj.mergefiles();
			response.setContentType("text/plain");
			if(result)
				pw.write("Files Are Merged");
			else
				pw.write("Error occured !!");
		}
		else
		{
			response.setStatus(401);
			pw.write(status);
		}
		pw.close();
		vobj.close();
		
		return null;
    }

}