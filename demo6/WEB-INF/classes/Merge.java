import java.util.*;
import java.io.*;
import java.sql.*; 


class Merge
{
	
	static Connection con=null;
	static Statement stmt = null;
	boolean join= false;
	ResultSet rs = null;
	boolean first=true;
	static File file1=null;
	static File file2=null;
	static Properties prop = null;
	InputStream input = null;
	Fileops fobj = null;
		

	void insert_to_DB(String hash)
	{  	
		try{ 		
			String table_name=prop.getProperty("tname");
			String query="insert into "+table_name+" values ('"+hash+"')";
			stmt.executeUpdate(query);
			}
		catch(Exception e)

		{ System.out.println("insertion problem "+e);}  
			  
	}

	void tablecreate(int flag)
	{
		
		try{

		fobj = new Fileops();
		Merge obj = new Merge();
		if (flag==1)
		{
			obj.start();
			System.out.println("merge invoked by add function");
		}
		
		DatabaseMetaData dbm = con.getMetaData();
		ResultSet tables = dbm.getTables(null, null, "hashes", null);		// checking if table already exists.
		String table_name=prop.getProperty("tname");

		if (tables.next()) {
			ResultSet rowcount=stmt.executeQuery("select count(*) from "+table_name);
			rowcount.next();
			if(rowcount.getInt(1)>0)
			{
				join=false;
				System.out.println("table exists");	
			}
			else
			{
				join=true;
				System.out.println("empty table exists");	
			}
			rowcount.close();
		}
		else
		{
			join=true;
			stmt.executeUpdate("create table "+table_name+"(hash varchar(100))");
			System.out.println("table created");	
		}
		tables.close();
		String line="";

		if(join){

			// BufferedReader br=new BufferedReader(new FileReader(file1));    // adding one.csv files to DB
			// while((line=br.readLine()) != null)
			// {	
			// 	obj.insert_to_DB(String.valueOf(line.hashCode()));
				
			// }
			// br.close();	
			fobj.createpath1(file1.getAbsolutePath());
			while((line = fobj.getfile1())!=null)
			{
				if((line.substring(line.length()-1)).equals("\n"))
					line=line.substring(0,line.length()-1);
				obj.insert_to_DB(String.valueOf(line.hashCode()));
			}
		}
		
		

	}
	catch(Exception e){e.printStackTrace();}
	}
	
	
	boolean mergefiles()
	{

		Merge obj = new Merge();
		try
		{
		fobj = new Fileops();
		obj.start();

		System.out.println("merge called");
		
		obj.tablecreate(0);
		String line="";
		//BufferedWriter bw=new BufferedWriter(new FileWriter(file1,true)); 
		fobj.createpath1(file2.getAbsolutePath());
		//BufferedReader br2=new BufferedReader(new FileReader(file2));	
		while((line=fobj.getfile1()) != null)
		{
			if((line.substring(line.length()-1)).equals("\n"))
				line=line.substring(0,line.length()-1);

			if(obj.check(String.valueOf(line.hashCode())) && line.length()>0)				//checking if duplicates occur
			{	
				System.out.println("new "+line);
				//bw.write(line);
				//bw.newLine();	
				String datas[] = line.split(",");
				fobj.addtofile(file1.getAbsolutePath(),datas[0],datas[1],datas[2]);											// unique values are updated to one.csv file
			}
		//	System.out.println("line length " +line.length());
		}
		obj.close();
		//bw.close();
		//br2.close();
		return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
	}

	boolean check(String hash)
	{
		String table_name=prop.getProperty("tname");
		boolean flag = false;
		try{
			Merge obj = new Merge();

			rs = stmt.executeQuery("select exists(select hash from "+table_name+" where hash = '" +hash+"')");
			rs.next();

			if(rs.getString(1).equals("0"))
			{		
				obj.insert_to_DB(hash);
				flag = true;
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return flag;

	}


	void start()
	{
		try
		{
		System.out.println("variables initialized");
		input= getClass().getClassLoader().getResourceAsStream("config.properties");
		prop = new Properties();
		prop.load(input);
		file1=new File(prop.getProperty("target"));
		file2=new File(prop.getProperty("source"));
		GetCon gobj=new GetCon();			//initialising connection variables.
		con = gobj.getConnection();
		stmt = con.createStatement();
		input.close();
		fobj = new Fileops();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	void close()
	{
		try {
			System.out.println("variables closed");
			stmt.close();
			con.close();

			//input.close();
			}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	

}  