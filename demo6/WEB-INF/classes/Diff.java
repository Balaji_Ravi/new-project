import java.util.*;
import java.io.*;
import java.util.Properties;

class Diff
{
			
	ArrayList<String> seq = null;
	ArrayList<String> remove=null;
	Diff(){
		}
	 public static void main(String[] args) {
	 	Diff obj = new Diff();
	 	System.out.println("hello\n"+obj.senddiff());
	 }
	
	public String senddiff()
	{
		Diff obj = new Diff();
		System.out.println("difference called");
		int olinecount=1;
		int nlinecount=1;
		ArrayList<Integer> checked=null;
		String json=null;
		BufferedReader br2=null;

		try{
		Properties prop = new Properties();
		InputStream input = getClass().getClassLoader().getResourceAsStream("config.cfg");
		prop.load(input);
		File oldf = new File(prop.getProperty("old"));
		File newf = new File(prop.getProperty("new"));

		BufferedReader br1=new BufferedReader(new FileReader(oldf));
	
		String oline="";
		String nline="";

		remove = new ArrayList<String>();
		seq = new ArrayList<String>();
		checked = new ArrayList<Integer>();
		RandomAccessFile raf = new RandomAccessFile(newf, "r");

		while((oline=br1.readLine()) != null)
		{
			nlinecount=1;
			boolean exists =false;
		//	br2=new BufferedReader(new FileReader(newf));
			while((nline=raf.readLine()) != null)
			{	
				if(oline.equals(nline) && !checked.contains(nlinecount))
				{
					seq.add(String.valueOf(nlinecount));
					checked.add(nlinecount);
					exists = true;
					break;
				}
				nlinecount++;
			}
			if(!exists)
			{
				remove.add(String.valueOf(olinecount));
			}
			olinecount++;
			raf.seek(0);
			//br2.close();
		}

		raf.close();
		br1.close();






















		/////////////////------json COnversion------------------///////////////

		json="{\"sequence\":"+seq+",\"remove\":"+remove+",\"old\":[";
		br2 = new BufferedReader(new FileReader(oldf));
		oline="";
		int count=0;
		int lastline = obj.nooflines(oldf);
		while((oline=br2.readLine())!=null)
		{
			json+="{\"value\":\""+String.valueOf(oline)+"\",\"index\":"+(count+1)+"}";
			if(count!=lastline)
				json+=",";
			count++;
		}
		br2.close();
		count=0;
		json+="],\"new\":[";

		br2 = new BufferedReader(new FileReader(newf));
		nline="";
		lastline = obj.nooflines(newf);
		while((nline=br2.readLine())!=null)
		{
			json+="{\"value\":\""+String.valueOf(nline)+"\",\"index\":"+(count+1)+"}";
			if(count!=lastline)
				json+=",";
			count++;
		}
		br2.close();
		json+="],";

		String message ="";
		if(remove.size()==0 && seq.size()==(lastline+1))
		{ 
			message ="Identitcal files";
		}
		else
			message = "Changes Occured";
		json+="\"message\":\""+message+"\"}";

	}
	catch(Exception e){e.printStackTrace();}
	return json;
		
}



int nooflines(File file)
{
	int lines =0;
	try{
	LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file));
    lineNumberReader.skip(Long.MAX_VALUE);
    lines = lineNumberReader.getLineNumber();
    lineNumberReader.close();
    
	}
	catch(Exception e){e.printStackTrace();}

	return lines;
}
}