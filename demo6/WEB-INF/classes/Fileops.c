#include<stdio.h>
#include<jni.h>
#include "Fileops.h"
#include<string.h>
#include<stdbool.h>
#include<stdlib.h>


void order(int*,int ,int );
int lines(const char*);
void copyele(int*, int*);
void add(int);
void seqchecker();
int getsize(int*);
void disp(int*);

int old_nooflines = 0;
int new_nooflines = 0;
int sequence[40][20]={0};
int rem[40]={0};
int seq[40]={0};
int finalseq[40]={0};


JNIEXPORT jint JNICALL Java_Fileops_getnooflines
  (JNIEnv *env, jobject obj, jstring path)
{
	const char *filepath = (*env)->GetStringUTFChars(env,path,0);
	//printf("filepath = %s\n",filepath);
	FILE *file = fopen(filepath,"r");
	int words=0;
	int count = 1;
	char c;
	if(file==NULL)
	{
		return -1;
	}
	else
	{
		while((c=fgetc(file))!=EOF)
		{
			words++;
			if(c =='\n')
				count++;
		}

	}
	fclose(file); 
	if(!words)
		count=0;
	else
		count--;
	//printf("count = ");

	
	return count;
}


JNIEXPORT jint JNICALL 
Java_Fileops_addtofile(JNIEnv *env, jobject obj, jstring path, jstring name, jstring id, jstring pwd)
{
	const char *filepath = (*env)->GetStringUTFChars(env,path,0);
	const char *name2 = (*env)->GetStringUTFChars(env,name,0);
	const char *id2 = (*env)->GetStringUTFChars(env,id,0);
	const char *pwd2 = (*env)->GetStringUTFChars(env,pwd,0);
	char data[300];
	FILE *file = fopen(filepath,"a");
	int created = 0;
	if(file==NULL)
	{
		created =1;
		return 0;
	}
	else
	{
	int count = 0;
	int c;

	data[0]='\0';
	// c=fgetc(file);
	// printf("%c this ftell position\n",c);
	// if(!created)
	// 	strcat(data,"\n");
	//else if(c!=NULL)
		//strcat(data,"\n");
	
	strcat(data,name2);
	strcat(data,",");
	strcat(data,id2);
	strcat(data,",");
	strcat(data,pwd2);
	strcat(data,"\n");	//to print on next Line

	printf("data = %s",data);
	fputs(data,file); 
	
	}
	fclose(file);
	return 1;

}


FILE *ptr = NULL;
JNIEXPORT void JNICALL 
Java_Fileops_createpath1(JNIEnv *env, jobject obj, jstring path)
{
	const char *filepath = (*env)->GetStringUTFChars(env,path,0);
	ptr = fopen(filepath,"r");

}



JNIEXPORT jstring JNICALL 
Java_Fileops_getfile1(JNIEnv *env, jobject obj)		//returns the file line by line
{
	char text[300];
	text[0]='\0';
	char *result;
	if(fgets(text,300,ptr)==NULL)
	{
		result = NULL;
		fclose(ptr);
	//f(fp);  
	}
	else
	{
		result = text;
	}
	return (*env)->NewStringUTF(env,result);
}


FILE *ptr2 = NULL;
JNIEXPORT void JNICALL 
Java_Fileops_createpath2(JNIEnv *env, jobject obj, jstring path)
{
	const char *filepath = (*env)->GetStringUTFChars(env,path,0);
	ptr2 = fopen(filepath,"r");

}



JNIEXPORT jstring JNICALL 
Java_Fileops_getfile2(JNIEnv *env, jobject obj)
{
	char text[300];
	text[0]='\0';
	char *result;
	if(fgets(text,300,ptr2)==NULL)
	{
		result = NULL;
		fclose(ptr2);
	}
	else
	{
		result = text;
	}
	return (*env)->NewStringUTF(env,result);

}


//////////////==========Difference finder===============/////////////////////////////

void disp(int *li)									// TO DISPLAY
{
	int i=0;
	printf("\nIN print ");
	for(i=0;li[i]!=0;i++)
		printf(" %d",li[i]);
	printf("\n");
}

void order(int *li, int prev_value, int prev_index)				//TO FIND THE SEQUENCES
{

	int *copy1 = malloc(40*sizeof(int));
	copyele(seq,copy1);
	bool flag = false;
	bool islastline = (prev_index == old_nooflines-2);
	int seqindex=0;
	int i;
	if(li[0] == -1 && islastline)
	{
		//printf("\nseq ="seq);
		seqchecker();
	}
	else if(li[0] == -1)
	{
		order(sequence[prev_index+2], prev_value, prev_index+1);
	}
	else if(islastline)
	{
	
		for(i=0;li[i]!=0;i++)
		{
			if(li[i] > prev_value )   
			{
				if(seq[0]==0)
					copyele(copy1,seq);	
				add(li[i]);
			}
			seqchecker();
		}

	}
	else
	{
		for(i=0;li[i]!=0;i++)
		{
			if(li[i] > prev_value)
			{	
				flag = true;
				if(seq[0]==0)
				{	
					copyele(copy1,seq);
				}
				add(li[i]);
				order(sequence[prev_index+2],li[i],prev_index+1);
			}
			
		}
		if(!flag)
				order(sequence[prev_index+2],prev_value,prev_index+1);
	}
	//for	
	free(copy1);
	
}

int lines(const char* filepath)				//GET THE NUMBER OF LINES
{
	FILE *file = fopen(filepath,"r");
	int words=0;
	int count = 1;
	char c;
	if(file == NULL)
	{
		return -1;
	}
	else
	{
		while((c=fgetc(file))!=EOF)
		{
			words++;
			if(c =='\n')
				count++;
		}

	}
	fclose(file); 
	if(!words)
		count=0;
	else
		count--;
	return count;
}

void copyele(int *src, int *dest) 						//TO COPY THE ARRAY ELEMENTS
{
	int i;
	memset(dest, 0,sizeof(int)*20);
	for(i=0;src[i]!=0;i++)
	{
		dest[i] = src[i];
	}
}

int lastelement = 0;
void add(int data)
{
	if(seq[0]==0)
		lastelement=0;
	seq[lastelement++]=data;
}

void seqchecker()
{														// SEQEUENCE CHECKER
	
	if(seq[0]!=0)
	{
		if(getsize(seq) > getsize(finalseq))
			{
			copyele(seq,finalseq);	
			//if( getsize(finalseq) == original_nooflines)
	 			//stop = true;	
			}
		else if(getsize(seq) == getsize(finalseq))
		{	

			int lastelement = seq[getsize(seq)-1];
			if(lastelement == new_nooflines && getsize(seq) == old_nooflines)
			{
				copyele(seq,finalseq);
			}
			
		}
	
	}
	//System.out.println(finalseq);
	int seqsize = getsize(seq);
	memset(seq, 0,sizeof(int)*seqsize);	
	
}

int getsize(int *arr)
{
	int count =0,i;
	for(i=0; arr[i]!=0; i++)
	{
		count++;
	}
	return count;
}


JNIEXPORT jintArray JNICALL 
Java_Fileops_getnewseq (JNIEnv *env, jobject obj, jstring old_path, jstring new_path)
{
	const char *oldpath = (*env)->GetStringUTFChars(env,old_path,0);					//GET THE SEQUENCE IN CHANGED TEXT AREA
	const char *newpath = (*env)->GetStringUTFChars(env,new_path,0);

	char oldtxt[100];
	char newtxt[100];
	int oldlines = lines(oldpath);
	old_nooflines = oldlines;
	FILE *old = fopen(oldpath,"r");
	int i,j;
	
	ptr2 = fopen(oldpath,"r");

	if(old!=NULL)
	{
		int oindex=0;
		FILE *new =  fopen(newpath,"r");
		oldtxt[0]='\0';
		while(fgets(oldtxt,100,old)!=NULL)
		{
			int nindex=0;
			int flag = 0;
			newtxt[0]='\0';
			int posindex=0;
			while(fgets(newtxt,100,new)!=NULL)
			{
				if(strcmp(oldtxt,newtxt)==0)
				{
					sequence[oindex][posindex++]=nindex+1;
					flag = 1;
				}
				nindex++;
			}
			fseek(new, 0, SEEK_SET);
			if(flag==0)
			{
					sequence[oindex][0]=-1;
					sequence[oindex][1]=0;
			}
			else
				sequence[oindex][posindex]=0;
			oindex++;
		}
		fclose(new);
	}
	fclose(old);
	printf("\n LE SEQUENCE\n");
	for(i=0;i<old_nooflines;i++)	//printing sequence
	{
		printf(" [");
		for(j=0;sequence[i][j]!=0;j++)
		{
			printf("%d ,",sequence[i][j]);			
		}
		printf("] ");
	}
	
	
	for(i=0;i<oldlines;i++)
	{

		int *first = sequence[i];
		int seqindex=0;
		if((first[0] == -1) && (i == (oldlines-1)))
		{
				break;
		}
		else if(i == (oldlines-1))
		{
			add(first[0]);
			seqchecker();
		}	
		else if((first[0]== -1))
			continue;
		else
		{
			for(j=0;first[j]!=0;j++)
			{
				//printf("\nstart ====== %d",first[j]);
				memset(seq, 0, sizeof(int)*20);		//clearing the array
				add(first[j]);
				order(sequence[i+1],first[j],i);
			}
		}
	}


	jintArray result;
	int size = getsize(finalseq);
	result = (*env)->NewIntArray(env,size);
	
	jint fill[size];
	for (i = 0; i < size; i++) {
	 fill[i] = finalseq[i]; 
	}
	// move from the temp structure to the java structure
	(*env)->SetIntArrayRegion(env, result, 0, size, fill);
	return result;
}

JNIEXPORT jintArray JNICALL Java_Fileops_getoldseq	       
  (JNIEnv *env , jobject obj)							//GET THE SEQUENCE IN ORIGINAL TEXT AREA
{
  	int ind = 0;
  	int i;
	for(i=0;i<old_nooflines;i++)			
	{
		int* lis = sequence[i];
		int j;
		for(j=0;j<getsize(lis);j++)
		{
			if(lis[j]==finalseq[ind])
			{
				
				rem[ind]=i+1;
				ind++;
				break;
			}
		}
	}

	jintArray result;
	int size = getsize(rem);
	result = (*env)->NewIntArray(env,size);
		
	jint fill[size];
	for (i = 0; i < size; i++) {
	 fill[i] = rem[i]; 
	}

	
	(*env)->SetIntArrayRegion(env, result, 0, size, fill);

	memset(seq, 0,sizeof(int)*40);				//SETTING ALL THE ARRAYS VALUES TO 0
	memset(rem, 0,sizeof(int)*40);
	memset(finalseq, 0,sizeof(int)*40);

	return result;


}

