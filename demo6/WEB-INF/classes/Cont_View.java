import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Properties;
import java.io.*;

public class Cont_View extends DispatchAction
{
    public ActionForward view(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        PrintWriter pw= response.getWriter();

		VerifyUser vobj = new VerifyUser();
		String token = request.getHeader("Authorization");
		String scope ="view";

		String status = "";
		status = vobj.verifytoken(token,scope);
		if(status.equals("ok"))
		{

			String path = request.getServletPath();
			path = path.substring(0,path.length()-3);
			System.out.println("view path ==== "+ path+" ====="+request.getServletPath());
			String split[] = path.split("/");
			int getline=0;
			boolean gotline=false;
			String json="";
			String filename = split[2];
			if(split.length>3){
				gotline=true;
				getline=Integer.parseInt(split[3]);
			}
			response.setContentType("text/plain");

			Fileops fobj = new Fileops();
			System.out.println("viewing called ");


			Properties prop = new Properties();
			//prop.load(new FileInputStream("config.cfg"));
			InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties");
			prop.load(input);

			String filepath="";
				if(filename.equals(prop.getProperty("sourcename"))||filename.equals(prop.getProperty("targetname")))
				{
					if(filename.equals(prop.getProperty("sourcename")))
						filepath=prop.getProperty("source");
					else
						filepath=prop.getProperty("target");

					//System.out.println(prop.getProperty("source"));
					File file1=new File(filepath);
					String line="";
					Converter conv = new Converter();
					Details det = new Details();

					//BufferedReader br = new BufferedReader(new FileReader(file1));
					int lines = 0;
						
					lines = fobj.getnooflines(filepath);
					
					int count=1;
					System.out.println("lines "+lines);
					boolean print=false;
					if(getline==0)
					{	
						print=true;
						pw.write('[');
					}
					fobj.createpath1(filepath);
					while((line=fobj.getfile1()) != null)
					{
						if((line.substring(line.length()-1)).equals("\n"))
							line=line.substring(0,line.length()-1);
						if(getline==count)
								print=true;
						if(line.length()>0 && print)
						{
							String s[]=line.split(",");
							det.name=s[0];
							det.id=s[1];
							det.pwd=s[2];
							json=conv.java2json_view(det);
							pw.write(json);
							//System.out.println("json "+json);
							if((count)!=lines && getline==0)
								pw.write(",");
							if(getline>0)
								break;
						}	
						count++;
					}
					conv.resetline();
					System.out.println("out "+ getline);
					if(getline==0)
					{
					System.out.println("I DID");
					pw.write(']');
					}		
				}
				else
				{
					
					response.setStatus(404);
					pw.write("wrong filename");
				}
			
		}
		else
		{
			response.setStatus(401);
			pw.write(status);
		}
		vobj.close();	
        pw.close();
        return null;
	
    }
}