import java.io.*;
import java.sql.*;
import java.util.Properties;

public class EditFile
{
    static String filename="";
    static String name="";
    static String pwd="";
    static String id="";
    static int line=0;
    static Properties prop = null;
    static String deletingline = null;


    public  String edit(String json)
    {
        try{
            Converter conv = new Converter();
            conv.json2java_edit(json);          //parses and sets the static values of the class
            String config_path = "D:\\Apache Software Foundation\\webapps\\demo6\\WEB-INF\\classes\\config.properties";
            FileInputStream input = new FileInputStream(config_path);
            prop = new Properties();
            prop.load(input);
            input.close();
            boolean db_change = false;
            String filepath = "";
            if(prop.getProperty("sourcename").equals(filename))
                filepath = prop.getProperty("source");
            else if(prop.getProperty("targetname").equals(filename))
            {filepath = prop.getProperty("target");
            db_change = true;
            }
            String csvdata=name+","+id+","+pwd;    
            new EditFile().insertStringInFile(new File(filepath),line,csvdata);
            if(db_change)
            {
                long hash = csvdata.hashCode();
                long existinghash = deletingline.hashCode();
                if(new EditFile().replace_in_db(hash,existinghash)>0)
                    return "ok";
                else
                    return "error";
            
            }
            return "ok";
           
        }    
        catch(Exception e)
        {
            e.printStackTrace();
            return "error";
        }
        
        
        
    }

    public void insertStringInFile(File inFile, int lineno, String lineToBeInserted) 
       throws Exception {
     // temp file
     File outFile = new File("$$$$$$$$.tmp");
     // input
     FileInputStream fis  = new FileInputStream(inFile);
     BufferedReader in = new BufferedReader(new InputStreamReader(fis));
     // output         
     FileOutputStream fos = new FileOutputStream(outFile);
     PrintWriter out = new PrintWriter(fos);
     
     String thisLine = "";
     int i =1;
     while ((thisLine = in.readLine()) != null) {
       if(i == lineno) 
        {
            out.println(lineToBeInserted);
            deletingline = thisLine;
        }
        else
        {
            out.println(thisLine);
        }  
       i++;
       }
    out.flush();
    out.close();
    in.close();
     
    inFile.delete();
    outFile.renameTo(inFile);
  }
public static void main(String[] args) {
    try{
    String config_path = "D:\\Apache Software Foundation\\webapps\\demo6\\WEB-INF\\classes\\config.properties";
        FileInputStream input = new FileInputStream(config_path);
        prop = new Properties();
        prop.load(input);
        input.close();
    new EditFile().replace_in_db(123456L,47536318L);
    }
    catch(Exception e){e.printStackTrace();}
}

  int replace_in_db(long newhash,long oldhash)
  {
    Connection con = null;
    try{
        con = new GetCon().getConnection();
        String table_name = prop.getProperty("tname");
        PreparedStatement pstmt = con.prepareStatement("update "+table_name+" set hash = ? where hash = ?");
        pstmt.setString(1,String.valueOf(newhash));
        pstmt.setString(2,String.valueOf(oldhash));
        System.out.println("query ===  "+pstmt.toString());
        int res = pstmt.executeUpdate();
        pstmt.close();
        con.close(); 
        return res;
    }
    catch(Exception e)
    {e.printStackTrace();
    return 0;}
  }
}