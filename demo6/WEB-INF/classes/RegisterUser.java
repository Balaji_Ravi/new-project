import java.util.*;

import com.mysql.jdbc.UpdatableResultSet;

import java.sql.*;

class RegisterUser
{
    static Connection con=null;
    static PreparedStatement pstmt = null;
    static ResultSet rs = null;
    public static String scopes = null;
    static{
        try{
        con = new GetCon().getConnection();
        }
        catch(Exception e){e.printStackTrace();}
    }
    boolean register(String client_id,String client_secret,String scope,String name)
    {
        int update_status1=1;
        int update_status2=1;
        try{
            pstmt=con.prepareStatement("insert into auth values(?,?,?,?)");
            pstmt.setString(1,client_id);
            pstmt.setString(2,client_secret);
            pstmt.setString(3,scope);
            pstmt.setString(4,name);

            update_status1 = pstmt.executeUpdate();

            pstmt = con.prepareStatement("insert into tokens(client_id) values(?)");
            pstmt.setString(1,client_id);
            update_status2 = pstmt.executeUpdate();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        if(update_status1>0 && update_status2>0)
            return true;
        return false;

    }

    String getid()
    {
        String id=null;
        try{
            while(true)
            {
                id = new Getcode().generatecode(6);
                pstmt= con.prepareStatement("select exists(select client_id from auth where client_id = ?)");
                pstmt.setString(1,id);
                rs = pstmt.executeQuery();
                rs.next();
                if(rs.getString(1).equals("0"))
                    break;
            }
            }
        catch(Exception e){e.printStackTrace();}
        return id;
    }

    String getsecret()
    {
        return new Getcode().generatecode(10);
    }

    boolean existinguser(String name)
    {
        try{
            pstmt = con.prepareStatement("select exists(select names from auth where names = ?)");
            pstmt.setString(1,name);
            rs = pstmt.executeQuery();
            rs.next();
            if(rs.getString(1).equals("1"))
             return true;
        }
        catch(Exception e){e.printStackTrace();}
        return false;
    }
}